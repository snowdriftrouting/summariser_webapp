import os

# export PDFBOX=/mnt/963GB/Data/Java/pdf_parsing/apache_pdfbox/2016/pdfbox-app-2.0.16.jar
BASE_PATH = os.path.dirname(os.path.abspath(__file__))

PDF_BOX_PATH = BASE_PATH+'/java/'
PDF_BOX_JAR = 'pdfbox-app-2.0.16.jar'
PDFBOX = PDF_BOX_PATH+PDF_BOX_JAR
UPLOAD_FOLDER = BASE_PATH+'/data/raw/'
PARSED_FOLDER = BASE_PATH+'/data/parsed/'
SEQ2SEQ_FOLDER = BASE_PATH+'/data/seq2seq_data/epoch12_batch167_total_batch9000/kp20k/subset/'
#20MB Max
MAX_UPLOAD_SIZE=20_000_000