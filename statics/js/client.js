function showPicker(inputId) { document.getElementById('docpicker').click(); }

function showPicked(input) {
    document.getElementById('upload-label').innerHTML = input.files[0].name;
    var reader = new FileReader();
    reader.onload = function (e) {
        document.getElementById('file-name').src = e.target.result;
    }
    reader.readAsDataURL(input.files[0]);
    document.getElementById("btnShort").disabled = false;
    document.getElementById("btnMedium").disabled = false;
    document.getElementById("btnLong").disabled = false;
    resetButtons();
}

function resetButtons() {
    const btnList = ['btnShort', 'btnMedium', 'btnLong'];
    for (const el of btnList) {
        if (document.getElementById(`${el}`).classList.contains('btn-success')) {
            document.getElementById(`${el}`).classList.remove('btn-success');
            document.getElementById(`${el}`).classList.add('btn-primary');
        }
    }
}

function shortSummary() {
    resetButtons()
    document.getElementById("btnShort").classList.remove('btn-primary');
    document.getElementById("btnShort").classList.add('btn-success');
    document.getElementById('result-area').innerHTML = '';
    summarise(2)
}

function mediumSummary() {
    resetButtons()
    document.getElementById("btnMedium").classList.remove('btn-primary');
    document.getElementById("btnMedium").classList.add('btn-success');
    document.getElementById('result-area').innerHTML = '';
    summarise(4)
}

function longSummary() {
    resetButtons()
    document.getElementById("btnLong").classList.remove('btn-primary');
    document.getElementById("btnLong").classList.add('btn-success');
    document.getElementById('result-area').innerHTML = '';
    summarise(8)
}

function show_pdf() {
    document.getElementById("pdf-area").innerHTML ='<object width="400" height="500" type="application/pdf" data="/my_pdf.pdf?#zoom=85&scrollbar=0&toolbar=0&navpanes=0"> <p>Insert your error message here, if the PDF cannot be displayed.</p> </object>'
}

function start_anim(){
    document.getElementById("loaderAnimation").classList.add("lds-facebook");
}

function stop_anim(){
    document.getElementById("loaderAnimation").classList.remove("lds-facebook");
}

function format_pred(pred){
    pred = pred.slice(2, -2);
    //var res = pred.split("<q>");
    var newline = String.fromCharCode(13, 10);
    var res = pred.replace(/<q-api>/g, newline);
    return res
}

function summarise(numSentences) {
    console.log('--summarise()')
    start_anim()
    var uploadFiles = document.getElementById('docpicker').files;
    if (uploadFiles.length != 1) alert('Please select 1 file to summarise.');

    var xhr = new XMLHttpRequest();
    var loc = window.location
    xhr.open('POST', `${loc.protocol}//${loc.hostname}:${loc.port}/summarise`, true);
    xhr.onerror = function() {alert (xhr.responseText);}
    xhr.onload = function(e) {
        if (xhr.status ==200){
            if (this.readyState === 4) {
                stop_anim()
                var response = JSON.parse(e.target.responseText);
                var payload = response['result'];
                var pred = payload[0];
                var res = format_pred(pred)
                document.getElementById('result-area').innerHTML = res;
            }
        }
        else if (xhr.status ==413){
            console.log(xhr.status)
            stop_anim();
            resetButtons();
            alert("Maximum upload size is 20MB");
        }
        else if (xhr.status ==411){
            console.log(xhr.status)
            stop_anim();
            resetButtons();
            alert("Browser compatibility issue, please use chrome, edge or firefox");
        }
        else {
            console.log(xhr.status)
            stop_anim();
            resetButtons();
            alert("There was an error processing your document. Please try with a different pdf");
        }
    }

    var fileData = new FormData();
    fileData.append('file', uploadFiles[0]);
    fileData.append('num_sentences', numSentences);
    xhr.send(fileData);
}

function keywordSummary(){
    var xhr = new XMLHttpRequest();
    var loc = window.location
    xhr.open('GET', `${loc.protocol}//${loc.hostname}:${loc.port}/keyword_summary`, true);
    xhr.onerror = function() {alert (xhr.responseText);}
    xhr.onload = function(e) {
        if (xhr.status ==200){
            if (this.readyState === 4) {
                var response = JSON.parse(e.target.responseText);
                var source = response['source'];
                var sourceText = source[0];
                var keywords = response['keywords'];
                keywords=keywords.toString().replace(/^\[([^]*)]$/,'$1')
                document.getElementById('keyword-source-area').innerHTML = sourceText;
                document.getElementById('keyword-result-area').innerHTML = keywords;
            }
        }
    }

    xhr.send();
}

