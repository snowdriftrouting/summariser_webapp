from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.requests import Request
from starlette.responses import HTMLResponse, JSONResponse
from starlette.middleware.cors import CORSMiddleware
from starlette.templating import Jinja2Templates
from middleware.contentlimit import LimitUploadSize
import uvicorn, aiohttp
import settings
import logging
from utils import setup_logging, check_file_type, persist_file, jar_wrapper, gen_summary, parse_keyword_file

logger = logging.getLogger(__name__)
setup_logging()

templates = Jinja2Templates(directory='templates')

app = Starlette(debug=True)
app.mount('/static', StaticFiles(directory='statics'), name='static')
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_headers=["X-Requested-With", "Content-Type"],
)
app.add_middleware(LimitUploadSize, max_upload_size=settings.MAX_UPLOAD_SIZE)

async def download_file(url, dest):
    if dest.exists(): return
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            data = await response.read()
            with open(dest, 'wb') as f: f.write(data)

@app.route('/')
async def homepage(request):
    template = "index.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context)

@app.route('/home')
async def homepage(request):
    template = "index.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context)

@app.route('/notify')
async def homepage(request):
    template = "notify.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context)

@app.route('/contact')
async def homepage(request):
    template = "contact.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context)

@app.route('/error')
async def error(request):
    """
    An example error. Switch the `debug` setting to see either tracebacks or 500 pages.
    """
    raise RuntimeError("Oh no")


@app.exception_handler(404)
async def not_found(request, exc):
    """
    Return an HTTP 404 page.
    """
    template = "404.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context, status_code=404)

@app.exception_handler(500)
async def server_error(request, exc):
    """
    Return an HTTP 500 page.
    """
    template = "500.html"
    context = {"request": request}
    return templates.TemplateResponse(template, context, status_code=500)

def format_response(summary):
    formatted={
            "result": [
                f"{summary}"
            ]
        }
    return formatted

def format_keyword_response(source, preds):
    formatted={
            "source": [
                f"{source}"
            ],
            "keywords": [
                f"{preds}"
            ]
        }
    return formatted

@app.route("/summarise", methods=["POST"])
async def summarise(request):
    ua = request.headers.get("User-Agent", "<unknown>")
    logger.info(f'--summarise client: {request.client}, ua: {ua}')
    form = await request.form()
    filename = form["file"].filename
    num_sentences = form["num_sentences"]
    # A SpooledTemporaryFile (a file-like object).
    # This is the actual Python file that you can pass directly to other functions or libraries that expect a "file-like" object.
    f = form["file"].file
    contents = await form["file"].read()
    persisted_file = persist_file(contents, filename)
    if persisted_file is not None:
        f_type = check_file_type(settings.UPLOAD_FOLDER+persisted_file)
        if f_type == 'pdf':
            p_name=persisted_file.split('.')[0]+'.txt'
            args = [f'{settings.PDF_BOX_PATH+settings.PDF_BOX_JAR}', 'ExtractText', f'{settings.UPLOAD_FOLDER+persisted_file}', '>', f'{settings.PARSED_FOLDER+p_name}']
            await jar_wrapper(*args)
            source_article, predicted_summary = await gen_summary(p_name, num_sentences)
            return JSONResponse(format_response(predicted_summary))
        elif f_type == 'doc':
            logger.debug('.doc file')
        else:
            logger.debug('not a .pdf or .doc file')
    return JSONResponse(format_response(''))

@app.route("/keyword_summary", methods=["GET"])
async def keyword_summary(request):
    ua = request.headers.get("User-Agent", "<unknown>")
    logger.info(f'--keyword_summary client: {request.client}, ua: {ua}')
    source, keywords=parse_keyword_file()
    if source is not None and keywords is not None:
        return JSONResponse(format_keyword_response(source, keywords))
    return JSONResponse(format_response(''))

if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=8000)
