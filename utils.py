import os
import logging
from subprocess import Popen, PIPE
from pathlib import Path
import magic
import uuid
import settings
import inspect
import os, random
import re
from bertsum.src.summarize import call_summarize

logger = logging.getLogger(__name__)

def setup_logging():
    level = logging.DEBUG
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
    handlers = [logging.FileHandler('logs/webapp.log'), logging.StreamHandler()]
    logging.basicConfig(level = level, format = format, handlers = handlers)

def byes_to_str(data):
    if isinstance(data, bytes):
        data = str(data, 'utf-8')
        return data
    return data

async def jar_wrapper(*args):
    logger.debug(f'>>jar_wrapper() args: {args}')
    process = Popen(['java', '-jar']+list(args), stdout=PIPE, stderr=PIPE)
    ret = []
    while process.poll() is None:
        line = process.stdout.readline()
        line=byes_to_str(line)
        if line != '' and line.endswith('\n'):
            ret.append(line[:-1])
    stdout, stderr = process.communicate()
    stdout = byes_to_str(stdout)
    stderr = byes_to_str(stderr)
    ret += stdout.split('\n')
    if stderr != '':
        ret += stderr.split('\n')
    ret.remove('')
    return ret


def check_file_type(filename):
    logger.debug(f'--check_file_type() filename: {filename}')
    fname = Path(filename)
    if fname.exists():
        f_ext = filename.split('.')[1]
        f_type = magic.from_buffer(open(filename, mode='rb').read(2048), mime=True)
        if f_ext == 'pdf' and f_type == 'application/pdf':
            return 'pdf'
        elif f_ext == 'doc' and f_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            return 'doc'
    else:
        return None

def persist_file(data, filename):
    '''Returns file name only'''
    # short unique enough id prefix
    uid = str(uuid.uuid4())[:8]
    p_name = uid+'_'+filename
    try:
        with open(settings.UPLOAD_FOLDER+p_name, "wb") as text_file:
            text_file.write(data)
            return p_name
    except Exception as e:
        return None

async def gen_summary(filename, top_n_sentences=3):
    logger.debug(f'>>gen_summary() filename: {filename}, top_n_sentences: {top_n_sentences}')
    try:
        with open(settings.PARSED_FOLDER+filename, 'r') as txt_file:
            text = txt_file.read()
            # results are formatted like this: results = {'source_article': source_article, 'predicted_summary': pred}
            results = call_summarize(text, top_n_sentences=top_n_sentences)
            source_article = results['source_article']
            predicted_summary = results['predicted_summary']
        return source_article, predicted_summary
    except FileNotFoundError as e:
        logger.error(e)
        return '','[[Server Error]]'

def inspect_obj(a):
    for i in inspect.getmembers(a):
        # Ignores anything starting with underscore
        # (that is, private and protected attributes)
        if not i[0].startswith('_') and not i[0] == 'auth':
            # Ignores methods
            if not inspect.ismethod(i[1]):
                print(i)

def parse_keyword_file(n_preds=10):
    random_file=random.choice(os.listdir(settings.SEQ2SEQ_FOLDER))
    logger.debug(f'--parse_keyword_file(): {random_file}')
    try:
        with open(settings.SEQ2SEQ_FOLDER+'/'+random_file, "r") as text_file:
            text=text_file.read()
            #first 8 chars is 'Source'
            source=text.split("Source Input:", 1)[0]
            source = source.split("]:", 1)[1]
            source = source.replace(' .', '.')
            source=source.strip().capitalize()
            source = '. '.join(map(lambda s: s.strip().capitalize(), source.split('.')))
            pred=text.split("[PREDICTION]", 1)[1]
            pred = pred.split("=======================================================", 1)[0]
            preds=pred.split("*[")
            top_n_preds = []
            i=0
            for p in preds:
                if i>0 and i <n_preds:
                    t = p.split("]")[0]
                    top_n_preds.append(t)
                i+=1
            return source, top_n_preds
    except Exception as e:
        return None, None
