import os

BERT_BASE_PATH = os.path.dirname(os.path.abspath(__file__))+'/'
MODELS_DIR = BERT_BASE_PATH+'models/bert_uci/'
MODEL = 'model_step_50000.pt'
MODEL_FP = f'{MODELS_DIR}{MODEL}'