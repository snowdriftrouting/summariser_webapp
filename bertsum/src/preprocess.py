#encoding=utf-8


import argparse
import time

from bertsum.src.others.logging import init_logger
from bertsum.src.prepro import data_builder


def do_format_to_lines(args):
    print(time.clock())
    data_builder.format_to_lines(args)
    print(time.clock())

def do_tokenize(args):
    print('>>do_tokenize')
    print(time.clock())
    data_builder.tokenize(args)
    print(time.clock())

def do_format_to_bert(args):
    print(time.clock())
    data_builder.format_to_bert(args)
    print(time.clock())




def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def args_workflow():
    parser = argparse.ArgumentParser()
    parser.add_argument("-mode", default='', type=str, help='format_to_lines or format_to_bert')
    parser.add_argument("-oracle_mode", default='greedy', type=str,
                        help='how to generate oracle summaries, greedy or combination, combination will generate more accurate oracles but take much longer time.')
    parser.add_argument("-map_path", default='../data/')
    parser.add_argument("-raw_path", default='../json_data/')
    parser.add_argument("-save_path", default='../bert_data/')

    parser.add_argument("-shard_size", default=2000, type=int)
    parser.add_argument('-min_nsents', default=3, type=int)
    parser.add_argument('-max_nsents', default=100, type=int)
    parser.add_argument('-min_src_ntokens', default=5, type=int)
    parser.add_argument('-max_src_ntokens', default=200, type=int)

    parser.add_argument("-lower", type=str2bool, nargs='?', const=True, default=True)

    parser.add_argument('-log_file', default='../../logs/bert.log')

    parser.add_argument('-dataset', default='', help='train, valid or test, defaul will process all datasets')

    parser.add_argument('-n_cpus', default=2, type=int)

    parser.add_argument('-fr_train', default=0.8, type=float)
    parser.add_argument('-fr_test', default=0.2, type=float)

    args = parser.parse_args()
    print(args)
    init_logger(args.log_file)
    eval('data_builder.' + args.mode + '(args)')

class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

def set_args_defaults():
    args = {
        'mode': '',
        'oracle_mode': 'greedy',
        'map_path': '../data/',
        'raw_path': '../json_data/',
        'save_path': '../bert_data/',
        'shard_size': 2000,
        'min_nsents': 3,
        'max_nsents': 100,
        'min_src_ntokens': 5,
        'max_src_ntokens': 200,
        'lower': True,
        'log_file': '../../logs/cnndm.log',
        'dataset': '',
        'n_cpus': 1,
        'fr_train': 0.8,
        'fr_test': 0.2
    }
    return args

def tokenize(args):
    args['mode'] = 'tokenize'
    #args['raw_path'] = '/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/parsed_to_text/'
    args['raw_path'] ='/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/raw_stories'
    #args['save_path'] = '/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/tokenized/'
    args['save_path'] = '/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/merged_stories_tokenized/'
    #args['log_file'] = '../logs/tokenize.log'
    args['log_file'] = '../logs/tokenize_temp.log'
    return args

def fmt_to_lines(args):
    args['mode'] = 'format_to_lines_split'
    #args['mode'] = 'format_to_lines'
    args['raw_path'] = '/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/tokenized/'
    #args['raw_path'] = '/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/merged_stories_tokenized/'
    args['save_path'] = '/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/json_data/'
    #args['save_path'] = '/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/json_data/cnndm/'
    #args['map_path'] = '/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/cnn/'
    args['log_file'] = '../logs/preproc_json.log'
    #args['log_file'] = '../logs/preproc_temp.log'
    return args

def fmt_to_bert(args):
    args['mode'] = 'format_to_bert'
    #args['raw_path'] = '/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/json_data/'
    args['raw_path'] = '/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/json_data/cnndm/'
    #args['save_path'] = '/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/bert_data/'
    args['save_path'] = '/mnt/nvme_1TB/Data/nlp/QandA/DeepMind/bert_data/'
    args['oracle_mode'] = 'greedy'
    args['n_cpus'] = 4
    #args['log_file'] = '../logs/preprocess.log'
    args['log_file'] = '../logs/preprocess_temp.log'
    return args

def args_to_dot(args):
    args = dotdict(args)
    args.tied = True
    print(args)
    return args

def hardcoded_workflow():
    args = set_args_defaults()
    #args = tokenize(args)
    args = fmt_to_lines(args)
    #args = fmt_to_bert(args)

    args = args_to_dot(args)
    init_logger(args.log_file)
    eval('data_builder.' + args.mode + '(args)')

if __name__ == '__main__':
    #hardcoded_workflow()
    args_workflow()

