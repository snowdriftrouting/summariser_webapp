


import glob
import os
import sys
import random
import signal
import time
import logging
from argparse import Namespace
from pathlib import Path

import torch
import nltk
nltk.download('punkt')

from pytorch_pretrained_bert import BertConfig

from bertsum.src.models import data_loader, model_builder
from bertsum.src.models.data_loader import load_dataset
from bertsum.src.models.model_builder import Summarizer
from bertsum.src.models.trainer import build_trainer
from bertsum.src.others.logging import logger, init_logger
from bertsum.src.prepro import data_builder
from bertsum.settings import BERT_BASE_PATH, MODELS_DIR, MODEL, MODEL_FP

logger = logging.getLogger(__name__)

#so pytorch can load model
sys.path.insert(0, BERT_BASE_PATH+'/src/')

#SAMPLE_DATA_FP = '/home/sample_data/cnndm.test.0.bert.pt'


# Setting these as default arguments
# These attributes can be edited as needed
args = Namespace(
    encoder="transformer",
    mode="test",
    bert_data_path=BERT_BASE_PATH+'bert_data',
    model_path=MODELS_DIR,
    result_path=BERT_BASE_PATH+'results',
    temp_dir=BERT_BASE_PATH+'temp',
    bert_config_path=BERT_BASE_PATH+'bert_config_uncased_base.json',
    batch_size=1000,
    model_fp=MODEL_FP,

    use_interval=True,
    hidden_size=128,
    ff_size=512, # Size used during training
    heads=4,
    inter_layers=2,
    rnn_size=512,

    param_init=0.0,
    param_init_glorot=True,
    dropout=0.1,
    optim='adam',
    lr=1,
    beta1= 0.9,
    beta2=0.999,
    decay_method='',
    warmup_steps=8000,
    max_grad_norm=0,

    save_checkpoint_steps=5,
    accum_count=1,
    world_size=1,
    report_every=1,
    train_steps=1000,
    recall_eval=False,


    visible_gpus='-1',
    gpu_ranks='0',
    log_file=BERT_BASE_PATH+'logs/summarize.log',
    dataset='',
    seed=666,

    test_all=False,
    test_from=MODEL,
    train_from='',
    report_rouge=True,
    block_trigram=True,
)

# Preprocessing arguments
# Setting these as default arguments
# These attributes can be edited as needed
pp_args = Namespace(
    mode="",
    oracle_mode='greedy',
    shard_size=2000,
    min_nsents=3,
    max_nsents=100,
    min_src_ntokens=5,
    max_src_ntokens=200,
    lower=True,
    dataset='',
    n_cpus=2
)

def summarize_text(src_str, args, pp_args, top_n_sentences=3, tgt_str='', summary_size=3):
    logger.debug(f'>>summarize_text() len(src_str): {len(src_str)}')
    '''
    Summarizes input text by returning the most important sentences based on the BERT model fine-tuned on CNN and Daily Mail articles
    '''
    cp = args.test_from
    step = int(cp.split('.')[-2].split('_')[-1])
    #Separate documents into list of sentences
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    src = [sent.split() for sent in nltk.tokenize.sent_tokenize(src_str)]
    tgt = [sent.split() for sent in nltk.tokenize.sent_tokenize(tgt_str)]
    bert = data_builder.BertData(pp_args)
    #doc_sent_list, abstract_sent_list, summary_size
    oracle_ids = data_builder.greedy_selection(src, tgt, summary_size)
    b_data = bert.preprocess(src, tgt, oracle_ids)
    indexed_tokens, labels, segments_ids, cls_ids, src_txt, tgt_txt = b_data
    b_dict = {"src": indexed_tokens, "labels": labels, "segs": segments_ids,
              'clss': cls_ids, 'src_str': src_txt, "tgt_str": tgt_txt}
    data = [[b_dict['src'], b_dict['labels'], b_dict['segs'], b_dict['clss'], b_dict['src_str'], b_dict['tgt_str']]]
    batch = data_loader.Batch(data, is_test=True, device=device)
    trained_model = torch.load(args.model_fp, map_location=lambda storage, loc: storage)
    # Read BERT config file
    # This contains information about the BERT model (e.g. hidden size for the transformer layers, number of transformer layers, number of self attention heads)
    config = BertConfig.from_json_file(args.bert_config_path)
    # Instantiate Summarizer and load pretrained model
    model = Summarizer(args, device, load_pretrained_bert=False, bert_config = config)
    model.load_cp(trained_model)
    device_id = 0 if device == "cuda" else -1
    trainer = build_trainer(args, device_id, model, None)
    results = trainer.example_api(batch, step, top_n_sentences=top_n_sentences, device=device)
    return results

def call_summarize(text, top_n_sentences, tgt_str=''):
    summary=summarize_text(text, args, pp_args, tgt_str=tgt_str, top_n_sentences=top_n_sentences)
    return summary

def test_summary(args, pp_args):
    # ## Sample run on Tiger Woods Wiki
    tiger_woods_wiki = '''Woods grew up in Orange County, California. He was a child prodigy who was introduced to golf before the age of two by his athletic father, Earl Woods. Earl was a single-digit handicap amateur golfer who also was one of the earliest African-American college baseball players at Kansas State University. Tiger's father was a member of the military and had playing privileges at the Navy golf course beside the Joint Forces Training Base in Los Alamitos, which allowed Tiger to play there. Tiger also played at the par 3 Heartwell golf course in Long Beach, as well as some of the municipals in Long Beach. In 1978, Tiger putted against comedian Bob Hope in a television appearance on The Mike Douglas Show. At age three, he shot a 48 over nine holes at the Navy course. At age five, he appeared in Golf Digest and on ABC's That's Incredible! Before turning seven, Tiger won the Under Age 10 section of the Drive, Pitch, and Putt competition, held at the Navy Golf Course in Cypress, California. In 1984 at the age of eight, he won the 9–10 boys' event, the youngest age group available, at the Junior World Golf Championships. He first broke 80 at age eight. He went on to win the Junior World Championships six times, including four consecutive wins from 1988 to 1991. Woods' father Earl wrote that Tiger first defeated him at the age of 11 years, with Earl trying his best. Earl lost to Tiger every time from then on. Woods first broke 70 on a regulation golf course at age 12. When Woods was 13 years old, he played in the 1989 Big I, which was his first major national junior tournament. In the final round, he was paired with pro John Daly, who was then relatively unknown. The event's format placed a professional with each group of juniors who had qualified. Daly birdied three of the last four holes to beat Woods by only one stroke. As a young teenager, Woods first met Jack Nicklaus in Los Angeles at the Bel-Air Country Club, when Nicklaus was performing a clinic for the club's members. Woods was part of the show, and he impressed Nicklaus and the crowd with his skills and potential. Earl Woods had researched in detail the career accomplishments of Nicklaus and had set his young son the goals of breaking those records. Woods was 15 years old and a student at Western High School in Anaheim when he became the youngest U.S. Junior Amateur champion; this was a record that stood until it was broken by Jim Liu in 2010. He was named 1991's Southern California Amateur Player of the Year (for the second consecutive year) and Golf Digest Junior Amateur Player of the Year. In 1992, he defended his title at the U.S. Junior Amateur Championship, becoming the tournament's first two-time winner. He also competed in his first PGA Tour event, the Nissan Los Angeles Open (he missed the 36-hole cut), and was named Golf Digest Amateur Player of the Year, Golf World Player of the Year, and Golfweek National Amateur of the Year. The following year, Woods won his third consecutive U.S. Junior Amateur Championship; he remains the event's only three-time winner. In 1994, at the TPC at Sawgrass in Florida, he became the youngest winner of the U.S. Amateur Championship, a record he held until 2008 when it was broken by Danny Lee. He was a member of the American team at the 1994 Eisenhower Trophy World Amateur Golf Team Championships (winning), and the 1995 Walker Cup (losing). Woods graduated from Western High School at age 18 in 1994 and was voted "Most Likely to Succeed" among the graduating class. He had starred for the high school's golf team under coach Don Crosby. Woods overcame difficulties with stuttering as a boy. This was not known until he wrote a letter to a boy who contemplated suicide. Woods wrote, "I know what it's like to be different and to sometimes not fit in. I also stuttered as a child and I would talk to my dog and he would sit there and listen until he fell asleep. I also took a class for two years to help me, and I finally learned to stop. '''
    summary=summarize_text(tiger_woods_wiki, args, pp_args, tgt_str='', top_n_sentences=3)
    logger.debug(f'summary: {summary}')

if __name__ == '__main__':
    test_summary(args, pp_args)