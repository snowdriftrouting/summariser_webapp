import xml.etree.ElementTree as ET
from pathlib import Path
import re,os
import glob
#from chardet.universaldetector import UniversalDetector
from bs4 import UnicodeDammit

#detector = UniversalDetector()

data_path='/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/fulltext/'
prep_path='/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/prep/'
out_path='/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/parsed_to_text/'


def pre_parse(in_file, out_dir):
    f = str(in_file).split('/')[-1]
    input = open(in_file, "r")
    output = open(out_dir+f, "w")

    for line in input:
        nl = line
        if nl.startswith('<catchphrase "id='):
            nl=nl.replace('<catchphrase "id=', '<catchphrase id="')
        elif nl.startswith('<sentence "id='):
            nl=nl.replace('<sentence "id=', '<sentence id="')
        elif nl.lstrip().startswith('AustLII: '):
            nl=''
        elif nl.lstrip().startswith('URL: http:'):
            nl = ''
        output.write(nl)

    input.close()
    output.close()

def parse_tree(pf):
    tree = ET.parse(pf)
    root = tree.getroot()
    highlights=[]
    sentences=[]
    for child in root:
        if child.tag=='catchphrases':
            for catchchild in child:
                highlights.append('\n\n')
                highlights.append('@highlight \n\n')
                t=catchchild.text
                t=t.replace("&eacute;",'\u00E9')
                highlights.append(t)
        elif child.tag=='name':
            t = child.text
            t = t.replace("&eacute;", '\u00E9')
            sentences.append(t)
        elif child.tag=='sentences':
            for schild in child:
                t = schild.text
                t = t.replace("&eacute;", '\u00E9')
                sentences.append(t)

    sents = ''.join(elem for elem in sentences)
    hts = ''.join(elem for elem in highlights)
    txt=sents+hts
    #reduce > 2 blank lines to 1
    txt=re.sub(r'\n\s*\n', '\n\n', txt)
    #print(txt)
    return txt

def ensure_unicode(fpth):
    '''open, decode, save'''
    output = open(str(fpth)+'_', "w")
    with open(str(fpth),'r',encoding='utf-8') as pf:
        s=pf.read()
        s=s.encode('utf-8')
        s=s.decode('utf-8')
        output.write(s)
        output.close()
    os.remove(fpth)
    os.rename(str(fpth)+'_', str(fpth))

def get_encoding(f):
    txt=f.read()
    d = UnicodeDammit(txt)
    print(f'{f} original_encoding: {d.original_encoding}')



def parse_xml_files(in_dir, out_dir):
    pathlist = Path(in_dir).glob('**/*.xml')
    for path in pathlist:
        #ensure_unicode(path)
        pre_parse(path, prep_path)
        fname = str(path).split('/')[-1]
        print(f'parsing: {fname}')
        with open(prep_path + fname) as pf:
            get_encoding(pf)
            txt=parse_tree(pf)
            fname = str(path).split('/')[-1]
            fname=fname.split('.xml')[0]
            with open(out_dir+'/'+fname+'.story', 'w') as outfile:
                outfile.write(txt)

if __name__ == '__main__':
    parse_xml_files(data_path, out_path)
