import xml.etree.ElementTree as ET
from pathlib import Path
import re,os,codecs
import glob
#from chardet.universaldetector import UniversalDetector
from bs4 import UnicodeDammit

#detector = UniversalDetector()

data_path='/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/fulltext/'
prep_path='/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/prep/'
out_path='/mnt/nvme_1TB/Data/nlp/uci/corpus/corpus/parsed_to_text/'

def pre_parse(in_file, out_dir):
    f = str(in_file).split('/')[-1]
    input = open(in_file, "r")
    output = open(out_dir+f, "w")
    try:
        for line in input:
            nl = line
            if nl.startswith('<catchphrase "id='):
                nl=nl.replace('<catchphrase "id=', '<catchphrase id="')
            elif nl.startswith('<sentence "id='):
                nl=nl.replace('<sentence "id=', '<sentence id="')
            elif nl.lstrip().startswith('AustLII: '):
                nl=''
            elif nl.lstrip().startswith('URL: http:'):
                nl = ''
            output.write(nl)
    except UnicodeDecodeError as e:
        #not worth the effort, forget these files
        pass
        '''
        print(f'--pre_parse(), {f}.xml: {e}')
        try:
            d=codecs.open(in_file, 'r', 'iso-8859-1').read()
            for nl in d:
                #ignore errors, force to utf-8
                if nl.startswith('<catchphrase "id='):
                    nl = nl.replace('<catchphrase "id=', '<catchphrase id="')
                elif nl.startswith('<sentence "id='):
                    nl = nl.replace('<sentence "id=', '<sentence id="')
                elif nl.lstrip().startswith('AustLII: '):
                    nl = ''
                elif nl.lstrip().startswith('URL: http:'):
                    nl = ''
                output.write(str(nl)+'\n')
        except UnicodeDecodeError as e:
            print(f'--pre_parse() {f}.xml nor latin-1 either: {e}')
        '''

    input.close()
    output.close()

def parse_logic(line):
    print(f'line: {line}')
    nl = line
    sentence=None
    highlight=None
    if nl.lstrip().startswith('<name>'):
        nl = nl.split('<name>')[1]
        nl = nl.split('</name>')[0]
        sentence=nl+'\n'
    elif  nl.lstrip().startswith('<catchphrase id'):
        nl = nl.split('>')[1]
        nl = nl.split('</catchphrase')[0]
        highlight='\n\n'+'@highlight \n\n'+nl
    elif nl.lstrip().startswith('<sentence id'):
        nl = nl.split('>')[1]
        nl = nl.split('</sentence')[0]
        sentence=nl+'\n'
    print(f'highlight: {highlight}, sentence: {sentence}')
    return highlight, sentence

def xparse(f,in_file, out_dir):
    input = open(in_file+f, "r")
    fo=f.split('.xml')[0]
    output = open(out_dir+fo+'.story', "w")
    highlights=[]
    sentences=[]
    clines = False
    sents = False
    cp = False
    sp = False
    #try:
    for line in input:
        if cp: clines=True
        if sp: sents = True
        nl = line
        if nl.lstrip().startswith('<name>'):
            nl = nl.split('<name>')[1]
            nl = nl.split('</name>')[0]
            sentences.append(nl+'\n')
        elif nl.lstrip().rstrip() == '<catchphrases>':
            cp = True
        elif nl.lstrip().rstrip() == '</catchphrases>':
            clines = False
            cp = False
        elif nl.lstrip().rstrip() == '<sentences>':
            sp = True
        elif nl.lstrip().rstrip() == '</sentences>':
            sents = False
            sp=False
        if clines:
            assert sents == False
            highlights.append('\n\n')
            highlights.append('@highlight \n\n')
            if nl.lstrip().startswith('<catchphrase'):
                nl = nl.split('">')[1]
            if nl.rstrip().endswith('</catchphrase>'):
                nl = nl.split('</catchphrase')[0]
            highlights.append(nl)
        elif sents:
            assert clines == False
            if nl.lstrip().startswith('<sentence'):
                nl = nl.split('">')[1]
            if nl.rstrip().endswith('</sentence>'):
                nl = nl.split('</sentence')[0]
            sentences.append(nl)
    sents = ''.join(elem for elem in sentences)
    hts = ''.join(elem for elem in highlights)
    txt=sents+hts
    #reduce > 2 blank lines to 1
    txt=re.sub(r'\n\s*\n', '\n\n', txt)

    output.write(txt)

    input.close()
    #print(f'wrote to: {str(output)}')
    output.close()


def parse_xml_files(in_dir, out_dir):
    pathlist = Path(in_dir).glob('**/*.xml')
    for path in pathlist:
        pre_parse(path, prep_path)
        f = str(path).split('/')[-1]
        xparse(f,prep_path, out_dir)


if __name__ == '__main__':
    parse_xml_files(data_path, out_path)
