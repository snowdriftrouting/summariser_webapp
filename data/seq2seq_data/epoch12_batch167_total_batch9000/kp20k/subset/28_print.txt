[Source][66]: context matters the structure of task goals affects accuracy in multiple target visual search . task structures typical of radiology and airport security searches are compared . participants searching in a radiology structure are to achieve a fixed objective . participants in an airport security structure search for a fixed duration . higher errors rates for multiple targets are seen in the fixed objective condition . 

Source Input: 
 <s> context matters the structure of task goals affects accuracy in multiple target visual search . task structures typical of radiology and airport security searches are compared . participants searching in a radiology structure are to achieve a fixed objective . participants in an airport security structure search for a fixed duration . higher errors rates for multiple targets are seen in the fixed objective condition . </s> <pad> <pad> <pad>
Real Target String [3] 
		[['visual', 'search'], ['radiology'], ['airport', 'security', 'screening']] 
Real Target Input:  
		[['<s>', 'visual', 'search', '</s>'], ['<s>', 'radiology', '</s>'], ['<s>', 'airport', 'security', 'screening', '</s>']] 
Real Target Copy:   
		[['visual', 'search', '</s>'], ['radiology', '</s>'], ['airport', 'security', 'screening', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=2/3
		[visual search]
		[radiology]
		airport security screening
oov_list:   
		[] 
[PREDICTION] #(valid)=108, #(present)=109, #(retained&present)=108, #(all)=109
		[4.7633]	*[decision support system] 	 [173, 130, 35, 2] 
		[4.8042]	*[computer animation] 	 [157, 1747, 2] 
		[5.1545]	*[non natives] 	 [123, 16916, 2] 
		[5.3278]	*[autopoiesis] 	 [8827, 2] 
		[5.5062]	*[decision making] 	 [173, 428, 2] 
		[5.6025]	*[multiple stressors] 	 [125, 14366, 2] 
		[5.6392]	*[comparative study] 	 [1637, 67, 2] 
		[5.6565]	*[cellular automata] 	 [940, 1064, 2] 
		[5.8823]	*[risk assessment] 	 [599, 671, 2] 
		[5.9617]	*[decision support systems] 	 [173, 130, 41, 2] 
		[5.9647]	*[e learning] 	 [129, 77, 2] 
		[6.0655]	*[quality of service] 	 [141, 6, 162, 2] 
		[6.1024]	*[decision support] 	 [173, 130, 2] 
		[6.1744]	*[virtual characters] 	 [312, 2910, 2] 
		[6.2142]	*[computer vision] 	 [157, 1051, 2] 
		[6.2274]	*[human computer interaction] 	 [252, 157, 253, 2] 
		[6.3493]	*[virtual screening] 	 [312, 2506, 2] 
		[6.4011]	*[decision analysis] 	 [173, 43, 2] 
		[6.4450]	*[self stabilization] 	 [339, 3091, 2] 
		[6.5188]	*[decision fusion] 	 [173, 1125, 2] 
		[6.5318]	*[learning] 	 [77, 2] 
		[6.5656]	*[knowledge acquisition] 	 [134, 1575, 2] 
		[6.6169]	*[supply chain] 	 [719, 596, 2] 
		[6.6586]	*[artificial neural network] 	 [661, 184, 52, 2] 
		[6.7231]	*[environmental pollution] 	 [1314, 3807, 2] 
		[6.7568]	*[human robot interaction] 	 [252, 629, 253, 2] 
		[6.7688]	*[self organization] 	 [339, 1252, 2] 
		[6.7869]	*[virtual humans] 	 [312, 2830, 2] 
		[7.0426]	*[fuzzy logic] 	 [83, 287, 2] 
		[7.0601]	*[supply chain management] 	 [719, 596, 144, 2] 
		[7.2812]	*[multi agent systems] 	 [100, 465, 41, 2] 
		[7.3886]	*[artificial neural networks] 	 [661, 184, 62, 2] 
		[7.4569]	*[knowledge based systems] 	 [134, 26, 41, 2] 
		[7.4843]	*[3d ladar scanning] 	 [305, 47817, 2770, 2] 
		[7.6011]	*[environmental impact assessment] 	 [1314, 474, 671, 2] 
		[7.6206]	*[decision] 	 [173, 2] 
		[7.7316]	*[risk] 	 [599, 2] 
		[7.8355]	*[knowledge based system] 	 [134, 26, 35, 2] 
		[7.9394]	*[multi agent] 	 [100, 465, 2] 
		[7.9558]	*[multi agent system] 	 [100, 465, 35, 2] 
		[7.9906]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[8.0255]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[8.0449]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[8.1657]	*[knowledge] 	 [134, 2] 
		[8.1673]	*[environmental impact] 	 [1314, 474, 2] 
		[8.2204]	*[quality] 	 [141, 2] 
		[8.2574]	*[learning in rank] 	 [77, 11, 1127, 2] 
		[8.2678]	*[multi agent simulation] 	 [100, 465, 93, 2] 
		[8.2852]	*[fuzzy logic controllers] 	 [83, 287, 2062, 2] 
		[8.3559]	*[adaptive] 	 [194, 2] 
		[8.4170]	*[information] 	 [47, 2] 
		[8.4744]	*[motion] 	 [321, 2] 
		[8.4946]	*[cellular] 	 [940, 2] 
		[8.5066]	*[learning in motion] 	 [77, 11, 321, 2] 
		[8.5730]	*[knowledge based animation] 	 [134, 26, 1747, 2] 
		[8.7886]	*[open] 	 [551, 2] 
		[8.7986]	*[human] 	 [252, 2] 
		[8.9677]	*[decision support simulation] 	 [173, 130, 93, 2] 
		[9.0246]	*[3d ladar disease] 	 [305, 47817, 1200, 2] 
		[9.0857]	*[video] 	 [215, 2] 
		[9.1348]	*[multi objective optimisation] 	 [100, 349, 1629, 2] 
		[9.1569]	*[supply chain system] 	 [719, 596, 35, 2] 
		[9.2038]	*[multi agent learning] 	 [100, 465, 77, 2] 
		[9.2274]	*[3d] 	 [305, 2] 
		[9.2624]	*[fuzzy logic system] 	 [83, 287, 35, 2] 
		[9.2770]	*[supply chain design] 	 [719, 596, 50, 2] 
		[9.3156]	*[multi objective learning] 	 [100, 349, 77, 2] 
		[9.4649]	*[medical] 	 [822, 2] 
		[9.6223]	*[computer] 	 [157, 2] 
		[9.6634]	*[knowledge based] 	 [134, 26, 2] 
		[9.8077]	*[multi objective] 	 [100, 349, 2] 
		[9.8386]	*[electronic] 	 [786, 2] 
		[10.0022]	*[human robot] 	 [252, 629, 2] 
		[10.0402]	*[e] 	 [129, 2] 
		[10.0495]	*[learning in the loop] 	 [77, 11, 4, 977, 2] 
		[10.0713]	*[environmental] 	 [1314, 2] 
		[10.2291]	*[fuzzy] 	 [83, 2] 
		[10.4600]	*[human computer] 	 [252, 157, 2] 
		[10.4693]	*[comparative] 	 [1637, 2] 
		[10.4736]	*[multi objective evolutionary algorithm] 	 [100, 349, 611, 37, 2] 
		[10.5693]	*[virtual] 	 [312, 2] 
		[10.7261]	*[computer aided] 	 [157, 2031, 2] 
		[11.1990]	*[3d ladar] 	 [305, 47817, 2] 
		[11.4424]	*[multi objective evolutionary] 	 [100, 349, 611, 2] 
		[11.4444]	*[multi objective evolutionary algorithms] 	 [100, 349, 611, 75, 2] 
		[11.5290]	*[learning in motion planning] 	 [77, 11, 321, 552, 2] 
		[11.5719]	*[quality of service metastasis] 	 [141, 6, 162, 9669, 2] 
		[11.6094]	*[learning in motion engineering] 	 [77, 11, 321, 416, 2] 
		[11.6514]	*[artificial neural] 	 [661, 184, 2] 
		[11.7147]	*[learning in the limit] 	 [77, 11, 4, 1278, 2] 
		[11.7208]	*[supply] 	 [719, 2] 
		[11.7521]	*[decision support system system] 	 [173, 130, 35, 35, 2] 
		[11.7879]	*[decision support system design] 	 [173, 130, 35, 50, 2] 
		[11.8660]	*[learning in motion realization] 	 [77, 11, 321, 3159, 2] 
		[11.9077]	*[decision support system development] 	 [173, 130, 35, 155, 2] 
		[12.0068]	*[supply chain management system] 	 [719, 596, 144, 35, 2] 
		[12.0198]	*[multi objective evolutionary learning] 	 [100, 349, 611, 77, 2] 
		[12.1624]	*[decision support system l*] 	 [173, 130, 35, 16983, 2] 
		[12.2141]	*[learning in the maxima] 	 [77, 11, 4, 7273, 2] 
		[12.2311]	*[decision support system models] 	 [173, 130, 35, 69, 2] 
		[12.7473]	*[learning in the action] 	 [77, 11, 4, 1063, 2] 
		[13.5846]	[learning in the <unk>] 	 [77, 11, 4, 3, 2] 
		[13.9632]	*[learning in the first] 	 [77, 11, 4, 105, 2] 
		[14.1395]	*[learning in the out] 	 [77, 11, 4, 326, 2] 
		[14.5400]	*[quality of service of] 	 [141, 6, 162, 6, 2] 
		[14.6321]	*[quality of service and] 	 [141, 6, 162, 8, 2] 
		[15.3810]	*[supply chain design and] 	 [719, 596, 50, 8, 2] 
		[15.7644]	*[learning in the e] 	 [77, 11, 4, 129, 2] 
		[16.8997]	*[decision support systems and] 	 [173, 130, 41, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.020689655172413796 , 0.013957307060755337 , 0.012800417972831766
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.020689655172413793 , 0.0409688013136289 , 0.02376365316474209
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.0662889994423965 , 0.07301587448233648 , 0.061686595044920745
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.056453202716235454 , 0.15699233968905824 , 0.07574310757051238
 =======================================================