[Source][237]: linkage problem , distribution estimation , and bayesian networks . this paper proposes an algorithm that uses an estimation of the joint distribution of promising solutions in order to generate new candidate solutions . the algorithm is settled into the context of genetic and evolutionary computation and the algorithms based on the estimation of distributions . the proposed algorithm is called the bayesian optimization algorithm ( boa ) . to estimate the distribution of promising solutions , the techniques for modeling multivariate data by bayesian networks are used . the boa identifies , reproduces , and mixes building blocks up to a specified order . it is independent of the ordering of the variables in strings representing the solutions . moreover , prior information about the problem can be incorporated into the algorithm , but it is not essential . first experiments were done with additively decomposable problems with both nonoverlapping as well as overlapping building blocks . the proposed algorithm is able to solve all but one of the tested problems in linear or close to linear time with respect to the problem size . except for the maximal order of interactions to be covered , the algorithm does not use any prior knowledge about the problem . the boa represents a step toward alleviating the problem of identifying and mixing building blocks correctly to obtain good solutions for problems with very limited domain information . 

Source Input: 
 <s> linkage problem , distribution estimation , and bayesian networks . this paper proposes an algorithm that uses an estimation of the joint distribution of promising solutions in order to generate new candidate solutions . the algorithm is settled into the context of genetic and evolutionary computation and the algorithms based on the estimation of distributions . the proposed algorithm is called the bayesian optimization algorithm ( boa ) . to estimate the distribution of promising solutions , the techniques for modeling multivariate data by bayesian networks are used . the boa identifies , reproduces , and mixes building blocks up to a specified order . it is independent of the ordering of the variables in strings representing the solutions . moreover , prior information about the problem can be incorporated into the algorithm , but it is not essential . first experiments were done with additively decomposable problems with both nonoverlapping as well as overlapping building blocks . the proposed algorithm is able to solve all but one of the tested problems in linear or close to linear time with respect to the problem size . except for the maximal order of interactions to be covered , the algorithm does not use any prior knowledge about the problem . the boa represents a step toward alleviating the problem of identifying and mixing building blocks correctly to obtain good solutions for problems with very limited domain information . </s> <pad> <pad> <pad>
Real Target String [6] 
		[['genetic', 'and', 'evolutionary', 'computation'], ['linkage', 'learning'], ['estimation', 'of', 'distribution', 'algorithm'], ['probabilistic', 'modeling'], ['learning', 'bayesian', 'networks'], ['genetic', 'algorithm']] 
Real Target Input:  
		[['<s>', 'genetic', 'and', 'evolutionary', 'computation', '</s>'], ['<s>', 'linkage', 'learning', '</s>'], ['<s>', 'estimation', 'of', 'distribution', 'algorithm', '</s>'], ['<s>', 'probabilistic', 'modeling', '</s>'], ['<s>', 'learning', 'bayesian', 'networks', '</s>'], ['<s>', 'genetic', 'algorithm', '</s>']] 
Real Target Copy:   
		[['genetic', 'and', 'evolutionary', 'computation', '</s>'], ['linkage', 'learning', '</s>'], ['estimation', 'of', 'distribution', 'algorithm', '</s>'], ['probabilistic', 'modeling', '</s>'], ['learning', 'bayesian', 'networks', '</s>'], ['genetic', 'algorithm', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=1/6
		[genetic and evolutionary computation]
		linkage learning
		estimation of distribution algorithm
		probabilistic modeling
		learning bayesian networks
		genetic algorithm
oov_list:   
		[] 
[PREDICTION] #(valid)=114, #(present)=114, #(retained&present)=114, #(all)=114
		[3.5967]	*[genetic algorithm] 	 [340, 37, 2] 
		[4.1783]	*[genetic algorithms] 	 [340, 75, 2] 
		[4.2925]	*[optimization] 	 [95, 2] 
		[4.3013]	*[neural networks] 	 [184, 62, 2] 
		[4.3046]	*[neural network] 	 [184, 52, 2] 
		[4.9456]	*[particle swarm optimization] 	 [689, 1313, 95, 2] 
		[5.0714]	*[classification] 	 [181, 2] 
		[5.1311]	*[uncertainty] 	 [685, 2] 
		[5.1967]	*[fuzzy logic] 	 [83, 287, 2] 
		[5.3177]	*[principal component analysis] 	 [1288, 414, 43, 2] 
		[5.3384]	*[markov random fields] 	 [752, 276, 842, 2] 
		[5.4351]	*[tracking] 	 [480, 2] 
		[5.6724]	*[evolutionary algorithm] 	 [611, 37, 2] 
		[5.7458]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[5.7999]	*[fuzzy set] 	 [83, 79, 2] 
		[5.8669]	*[evolutionary algorithms] 	 [611, 75, 2] 
		[5.9415]	*[missing data] 	 [1923, 29, 2] 
		[6.0148]	*[adaptive control] 	 [194, 66, 2] 
		[6.2227]	*[selection combining] 	 [240, 1022, 2] 
		[6.2841]	*[adaptive meshing] 	 [194, 6276, 2] 
		[6.2935]	*[graph recoloring] 	 [204, 14467, 2] 
		[6.3258]	*[gibbs sampling] 	 [4924, 711, 2] 
		[6.3305]	*[decision making] 	 [173, 428, 2] 
		[6.3920]	*[k means] 	 [203, 413, 2] 
		[6.4047]	*[graph algorithms] 	 [204, 75, 2] 
		[6.4564]	*[fuzzy logic controllers] 	 [83, 287, 2062, 2] 
		[6.5004]	*[convex optimization] 	 [881, 95, 2] 
		[6.5118]	*[graph coloring] 	 [204, 2300, 2] 
		[6.5330]	*[markov chain monte carlo] 	 [752, 596, 1225, 1226, 2] 
		[6.5540]	*[feature extraction] 	 [187, 591, 2] 
		[6.5661]	*[linear discriminant analysis] 	 [109, 1785, 43, 2] 
		[6.5843]	*[fuzzy clustering] 	 [83, 274, 2] 
		[6.6586]	*[feature selection] 	 [187, 240, 2] 
		[6.6638]	*[convex hull] 	 [881, 3335, 2] 
		[6.6880]	*[estimation] 	 [223, 2] 
		[6.8593]	*[decision support system] 	 [173, 130, 35, 2] 
		[7.2002]	*[adaptive] 	 [194, 2] 
		[7.2097]	*[particle swarm optimizers] 	 [689, 1313, 10752, 2] 
		[7.2892]	*[markov chain] 	 [752, 596, 2] 
		[7.7208]	*[fuzzy logic system] 	 [83, 287, 35, 2] 
		[7.7477]	*[estimation of distribution algorithms] 	 [223, 6, 207, 75, 2] 
		[7.8002]	*[k means clustering] 	 [203, 413, 274, 2] 
		[7.8563]	*[graph] 	 [204, 2] 
		[7.9204]	*[particle swarm optimisation] 	 [689, 1313, 1629, 2] 
		[8.0868]	*[selection] 	 [240, 2] 
		[8.1228]	*[markov random field] 	 [752, 276, 236, 2] 
		[8.1450]	*[fuzzy logic systems] 	 [83, 287, 41, 2] 
		[8.1997]	*[feature] 	 [187, 2] 
		[8.2826]	*[decision support systems] 	 [173, 130, 41, 2] 
		[8.3163]	*[multi objective optimisation] 	 [100, 349, 1629, 2] 
		[8.3549]	*[fuzzy] 	 [83, 2] 
		[8.4229]	*[neural] 	 [184, 2] 
		[8.4426]	*[fuzzy logic programming] 	 [83, 287, 160, 2] 
		[8.4803]	*[fuzzy random fields] 	 [83, 276, 842, 2] 
		[8.5634]	*[neural network analysis] 	 [184, 52, 43, 2] 
		[8.5648]	*[fuzzy random forest] 	 [83, 276, 2846, 2] 
		[8.6113]	*[missing] 	 [1923, 2] 
		[8.7295]	*[nonlinear] 	 [271, 2] 
		[8.8458]	*[k means algorithm] 	 [203, 413, 37, 2] 
		[8.8534]	*[decision support] 	 [173, 130, 2] 
		[8.8810]	*[neural network models] 	 [184, 52, 69, 2] 
		[8.9870]	*[neural network model] 	 [184, 52, 30, 2] 
		[8.9894]	*[neural network algorithm] 	 [184, 52, 37, 2] 
		[8.9895]	*[neural network estimation] 	 [184, 52, 223, 2] 
		[8.9898]	*[evolutionary] 	 [611, 2] 
		[9.0324]	*[fuzzy logic controller] 	 [83, 287, 767, 2] 
		[9.0455]	*[neural network optimization] 	 [184, 52, 95, 2] 
		[9.0828]	*[decision] 	 [173, 2] 
		[9.1635]	*[genetic] 	 [340, 2] 
		[9.2157]	*[vector] 	 [310, 2] 
		[9.2345]	*[soft] 	 [1048, 2] 
		[9.2437]	*[stochastic] 	 [529, 2] 
		[9.2453]	*[bayesian] 	 [824, 2] 
		[9.3397]	*[markov] 	 [752, 2] 
		[9.3826]	*[multi objective evolutionary algorithm] 	 [100, 349, 611, 37, 2] 
		[9.4622]	*[fuzzy random neighbor] 	 [83, 276, 1932, 2] 
		[9.8470]	*[linear] 	 [109, 2] 
		[9.8849]	*[fuzzy logic neural network] 	 [83, 287, 184, 52, 2] 
		[9.9695]	*[linear discriminant] 	 [109, 1785, 2] 
		[10.0094]	*[particle] 	 [689, 2] 
		[10.0591]	*[gibbs] 	 [4924, 2] 
		[10.1777]	*[fuzzy logic neural networks] 	 [83, 287, 184, 62, 2] 
		[10.2178]	*[principal component] 	 [1288, 414, 2] 
		[10.2610]	*[particle swarm] 	 [689, 1313, 2] 
		[10.2814]	*[estimation of distribution algorithm] 	 [223, 6, 207, 37, 2] 
		[10.4134]	*[particle swarm optimization algorithm] 	 [689, 1313, 95, 37, 2] 
		[10.4706]	*[multi objective evolutionary algorithms] 	 [100, 349, 611, 75, 2] 
		[10.5145]	*[estimation of distribution] 	 [223, 6, 207, 2] 
		[10.5811]	*[multi objective] 	 [100, 349, 2] 
		[11.0049]	*[active] 	 [573, 2] 
		[11.0436]	*[multi objective optimization algorithm] 	 [100, 349, 95, 37, 2] 
		[11.1868]	*[particle swarm optimization algorithms] 	 [689, 1313, 95, 75, 2] 
		[11.2823]	*[multi objective optimization algorithms] 	 [100, 349, 95, 75, 2] 
		[11.3097]	*[multi objective evolutionary optimization] 	 [100, 349, 611, 95, 2] 
		[11.3594]	*[fuzzy random forest model] 	 [83, 276, 2846, 30, 2] 
		[11.3674]	*[convex] 	 [881, 2] 
		[11.6599]	*[fuzzy random] 	 [83, 276, 2] 
		[11.6951]	*[markov random field model] 	 [752, 276, 236, 30, 2] 
		[11.7068]	*[estimation of distribution optimization] 	 [223, 6, 207, 95, 2] 
		[11.7195]	*[multi objective optimization problem] 	 [100, 349, 95, 44, 2] 
		[11.7220]	*[principal component analysis algorithm] 	 [1288, 414, 43, 37, 2] 
		[11.7684]	*[principal component analysis algorithms] 	 [1288, 414, 43, 75, 2] 
		[11.9382]	*[estimation of distribution parameters] 	 [223, 6, 207, 196, 2] 
		[11.9852]	*[estimation of distribution system] 	 [223, 6, 207, 35, 2] 
		[11.9871]	*[fuzzy random neighbor model] 	 [83, 276, 1932, 30, 2] 
		[12.0026]	*[particle swarm optimization problem] 	 [689, 1313, 95, 44, 2] 
		[12.1878]	*[multi objective evolutionary] 	 [100, 349, 611, 2] 
		[12.2475]	*[multi objective evolutionary programming] 	 [100, 349, 611, 160, 2] 
		[12.3560]	*[markov random] 	 [752, 276, 2] 
		[12.4914]	*[fuzzy random neighbor classifier] 	 [83, 276, 1932, 879, 2] 
		[14.2685]	*[fuzzy logic neural] 	 [83, 287, 184, 2] 
		[15.0132]	*[markov random fields and] 	 [752, 276, 842, 8, 2] 
		[15.7156]	*[neural networks and fuzzy] 	 [184, 62, 8, 83, 2] 
		[16.2714]	*[particle swarm optimization and] 	 [689, 1313, 95, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.02580645161290323 , 0.03338453661034306 , 0.02535584452092991
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.016129032258064516 , 0.04180747567844342 , 0.020901697811714793
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0800000011920929 , 0.4000000059604645 , 0.13333333532015484
 --- total precision, recall, fscore: 0.04898617552813663 , 0.07022956199123806 , 0.05210785785047631
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.04000000059604645 , 0.4000000059604645 , 0.07272727381099355
 --- total precision, recall, fscore: 0.035381464772326975 , 0.11105137525714792 , 0.049815173110397334
 =======================================================