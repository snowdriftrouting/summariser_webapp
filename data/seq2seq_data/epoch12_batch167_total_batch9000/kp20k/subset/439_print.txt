[Source][72]: novel adaptive bacterial foraging algorithms for global optimisation with application to modelling of a trs . first , we proposed abfa based on index of iteration . second , we proposed abfa based on synergy of iteration index and fitness cost . the algorithms are tested with standard benchmark functions . the algorithms are employed to optimise a nn model to represent a trs . the proposed algorithms outperformed standard bfa . 

Source Input: 
 <s> novel adaptive bacterial foraging algorithms for global optimisation with application to modelling of a trs . first , we proposed <unk> based on index of iteration . second , we proposed <unk> based on synergy of iteration index and fitness cost . the algorithms are tested with standard benchmark functions . the algorithms are employed to optimise a nn model to represent a trs . the proposed algorithms outperformed standard bfa . </s> <pad> <pad> <pad>
Real Target String [4] 
		[['adaptive', 'bacterial', 'foraging'], ['optimisation', 'algorithm'], ['nonparametric', 'modelling'], ['twin', 'rotor', 'system']] 
Real Target Input:  
		[['<s>', 'adaptive', 'bacterial', 'foraging', '</s>'], ['<s>', 'optimisation', 'algorithm', '</s>'], ['<s>', 'nonparametric', 'modelling', '</s>'], ['<s>', 'twin', 'rotor', 'system', '</s>']] 
Real Target Copy:   
		[['adaptive', 'bacterial', 'foraging', '</s>'], ['optimisation', 'algorithm', '</s>'], ['nonparametric', 'modelling', '</s>'], ['twin', 'rotor', 'system', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=1/4
		[adaptive bacterial foraging]
		optimisation algorithm
		nonparametric modelling
		twin rotor system
oov_list:   
		['abfa'] 
[PREDICTION] #(valid)=115, #(present)=115, #(retained&present)=115, #(all)=115
		[4.8553]	*[genetic algorithm] 	 [340, 37, 2] 
		[5.2203]	*[genetic algorithms] 	 [340, 75, 2] 
		[5.2594]	*[neural networks] 	 [184, 62, 2] 
		[5.2931]	*[neural network] 	 [184, 52, 2] 
		[5.5429]	*[tomography] 	 [2160, 2] 
		[5.5954]	*[adaptive meshing] 	 [194, 6276, 2] 
		[5.6239]	*[optimization] 	 [95, 2] 
		[5.7047]	*[particle swarm optimization] 	 [689, 1313, 95, 2] 
		[5.7694]	*[uncertainty] 	 [685, 2] 
		[5.9441]	*[fuzzy logic] 	 [83, 287, 2] 
		[6.2495]	*[adaptive control] 	 [194, 66, 2] 
		[6.3716]	*[fuzzy set] 	 [83, 79, 2] 
		[6.3771]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[6.4676]	*[decision making] 	 [173, 428, 2] 
		[6.7158]	*[computer animation] 	 [157, 1747, 2] 
		[6.7388]	*[computational complexity] 	 [201, 211, 2] 
		[6.7861]	*[soft clay] 	 [1048, 8005, 2] 
		[6.8130]	*[fuzzy sets] 	 [83, 229, 2] 
		[6.8952]	*[iterative methods] 	 [764, 74, 2] 
		[6.8984]	*[bzier curves] 	 [7307, 1316, 2] 
		[6.9157]	*[fuzzy logic controllers] 	 [83, 287, 2062, 2] 
		[6.9459]	*[adaptive algorithms] 	 [194, 75, 2] 
		[6.9824]	*[convex optimization] 	 [881, 95, 2] 
		[7.0423]	*[particle swarm optimizers] 	 [689, 1313, 10752, 2] 
		[7.0467]	*[convex hull] 	 [881, 3335, 2] 
		[7.0844]	*[particle filter] 	 [689, 483, 2] 
		[7.1023]	*[nonlinear programming] 	 [271, 160, 2] 
		[7.1185]	*[particle filtering] 	 [689, 800, 2] 
		[7.1211]	*[bzier curve] 	 [7307, 1014, 2] 
		[7.1888]	*[nonlinear systems] 	 [271, 41, 2] 
		[7.1974]	*[adaptive] 	 [194, 2] 
		[7.2142]	*[computer vision] 	 [157, 1051, 2] 
		[7.3847]	*[active contour] 	 [573, 1832, 2] 
		[7.4804]	*[adaptive learning] 	 [194, 77, 2] 
		[7.5462]	*[decision support system] 	 [173, 130, 35, 2] 
		[7.6246]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[8.1363]	*[particle bee] 	 [689, 5922, 2] 
		[8.2464]	*[fuzzy logic system] 	 [83, 287, 35, 2] 
		[8.3188]	*[nonlinear] 	 [271, 2] 
		[8.3706]	*[power] 	 [107, 2] 
		[8.3951]	*[particle swarm optimisation] 	 [689, 1313, 1629, 2] 
		[8.6817]	*[iterative] 	 [764, 2] 
		[8.7469]	*[fuzzy logic systems] 	 [83, 287, 41, 2] 
		[8.8810]	*[multi objective optimisation] 	 [100, 349, 1629, 2] 
		[8.8828]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[8.9223]	*[shape] 	 [334, 2] 
		[8.9300]	*[decision support systems] 	 [173, 130, 41, 2] 
		[9.0140]	*[neural] 	 [184, 2] 
		[9.0281]	*[vessel] 	 [5136, 2] 
		[9.0365]	*[fuzzy logic programming] 	 [83, 287, 160, 2] 
		[9.1187]	*[3d] 	 [305, 2] 
		[9.2630]	*[active contour models] 	 [573, 1832, 69, 2] 
		[9.3663]	*[mathematical] 	 [743, 2] 
		[9.3954]	*[decision support] 	 [173, 130, 2] 
		[9.3978]	*[bzier] 	 [7307, 2] 
		[9.4113]	*[decision] 	 [173, 2] 
		[9.4389]	*[cytological] 	 [24820, 2] 
		[9.4967]	*[neural network analysis] 	 [184, 52, 43, 2] 
		[9.4998]	*[bayesian] 	 [824, 2] 
		[9.5298]	*[fuzzy logic controller] 	 [83, 287, 767, 2] 
		[9.6025]	*[multi objective programming] 	 [100, 349, 160, 2] 
		[9.7576]	*[particle bee functions] 	 [689, 5922, 171, 2] 
		[9.7650]	*[active contour model] 	 [573, 1832, 30, 2] 
		[9.7812]	*[particle] 	 [689, 2] 
		[9.8236]	*[particle bee function] 	 [689, 5922, 114, 2] 
		[9.8542]	*[fuzzy set models] 	 [83, 79, 69, 2] 
		[9.8670]	*[linear] 	 [109, 2] 
		[10.0125]	*[soft] 	 [1048, 2] 
		[10.0126]	*[nuclei] 	 [6272, 2] 
		[10.0441]	*[fuzzy logic methods] 	 [83, 287, 74, 2] 
		[10.0856]	*[fuzzy set theory] 	 [83, 79, 179, 2] 
		[10.0903]	*[fuzzy logic models] 	 [83, 287, 69, 2] 
		[10.1937]	*[genetic] 	 [340, 2] 
		[10.3941]	*[multi objective evolutionary algorithm] 	 [100, 349, 611, 37, 2] 
		[10.4934]	*[gibbs] 	 [4924, 2] 
		[10.6901]	*[pearson] 	 [7209, 2] 
		[10.7040]	*[fuzzy logic neural network] 	 [83, 287, 184, 52, 2] 
		[10.7602]	*[particle swarm] 	 [689, 1313, 2] 
		[10.7987]	*[particle swarm neural network] 	 [689, 1313, 184, 52, 2] 
		[10.8944]	*[multi objective neural network] 	 [100, 349, 184, 52, 2] 
		[10.9573]	*[multi objective evolutionary algorithms] 	 [100, 349, 611, 75, 2] 
		[10.9667]	*[particle swarm neural networks] 	 [689, 1313, 184, 62, 2] 
		[11.0115]	*[multi objective] 	 [100, 349, 2] 
		[11.0693]	*[fuzzy logic neural networks] 	 [83, 287, 184, 62, 2] 
		[11.0984]	*[multi objective neural networks] 	 [100, 349, 184, 62, 2] 
		[11.4145]	*[particle swarm optimization algorithm] 	 [689, 1313, 95, 37, 2] 
		[11.5077]	*[non reflecting stationary logics] 	 [123, 4151, 1882, 2820, 2] 
		[11.6301]	*[computer aided] 	 [157, 2031, 2] 
		[11.8167]	*[computer] 	 [157, 2] 
		[11.8236]	*[multi objective evolutionary optimization] 	 [100, 349, 611, 95, 2] 
		[11.9186]	*[non reflecting stationary set] 	 [123, 4151, 1882, 79, 2] 
		[12.0158]	*[non reflecting stationary decomposition] 	 [123, 4151, 1882, 634, 2] 
		[12.1664]	*[multi objective optimization problem] 	 [100, 349, 95, 44, 2] 
		[12.2565]	*[particle swarm optimization algorithms] 	 [689, 1313, 95, 75, 2] 
		[12.2718]	*[multi objective evolutionary] 	 [100, 349, 611, 2] 
		[12.2934]	*[non reflecting stationary functions] 	 [123, 4151, 1882, 171, 2] 
		[12.3123]	*[multi objective optimization algorithm] 	 [100, 349, 95, 37, 2] 
		[12.3611]	*[multi objective optimization algorithms] 	 [100, 349, 95, 75, 2] 
		[12.3807]	*[non reflecting stationary condition] 	 [123, 4151, 1882, 609, 2] 
		[12.4845]	*[particle swarm optimization problem] 	 [689, 1313, 95, 44, 2] 
		[12.5120]	*[non reflecting stationary approximation] 	 [123, 4151, 1882, 320, 2] 
		[12.6142]	*[non reflecting stationary] 	 [123, 4151, 1882, 2] 
		[12.7342]	*[non reflecting stationary optimization] 	 [123, 4151, 1882, 95, 2] 
		[12.7788]	*[non reflecting stationary learning] 	 [123, 4151, 1882, 77, 2] 
		[13.2688]	*[non reflecting stationary logic] 	 [123, 4151, 1882, 287, 2] 
		[14.9923]	*[fuzzy logic neural] 	 [83, 287, 184, 2] 
		[15.3280]	*[neural networks and alternatives] 	 [184, 62, 8, 2138, 2] 
		[16.1277]	*[particle swarm neural] 	 [689, 1313, 184, 2] 
		[16.2752]	*[multi objective neural] 	 [100, 349, 184, 2] 
		[16.6738]	*[computer aided design and] 	 [157, 2031, 50, 8, 2] 
		[16.9582]	*[neural networks and fuzzy] 	 [184, 62, 8, 83, 2] 
		[17.0759]	*[non reflecting stationary linear] 	 [123, 4151, 1882, 109, 2] 
		[17.3322]	*[fuzzy logic of neural] 	 [83, 287, 6, 184, 2] 
		[17.6172]	*[non reflecting stationary neural] 	 [123, 4151, 1882, 184, 2] 
		[18.1818]	*[non reflecting stationary decision] 	 [123, 4151, 1882, 173, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.02681818181818182 , 0.03813169154078245 , 0.028154930421940696
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.01818181818181818 , 0.04801162642071733 , 0.023525910877147355
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.0505753972076557 , 0.07734288023159744 , 0.0550429675741054
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.025 , 0.25 , 0.045454545454545456
 --- total precision, recall, fscore: 0.040464105699211356 , 0.12109231492367475 , 0.055324449424871136
 =======================================================