[Source][123]: self stabilizing symmetry breaking in constant space ( extended abstract ) . we investigate the problem of self stabilizing round robin token management scheme on an anonymous bidirectional ring of identical processors , where each processor is an asynchronous probabilistic ( coin flipping ) finite state machine which sends and receives messages . we show that the solution to this problem is equivalent to symmetry breaking ( i . e . , leader election ) . requiring only constant size messages and message passing model has practical implications our solution can be implemented in high speed networks using a universal fast hardware switches ( i . e . , finite state machines ) of size independent of the size of the network . 

Source Input: 
 <s> self stabilizing symmetry breaking in constant space ( extended abstract ) . we investigate the problem of self stabilizing round robin token management scheme on an anonymous bidirectional ring of identical processors , where each processor is an asynchronous probabilistic ( coin flipping ) finite state machine which sends and receives messages . we show that the solution to this problem is equivalent to symmetry breaking ( i . e . , leader election ) . requiring only constant size messages and message passing model has practical implications our solution can be implemented in high speed networks using a universal fast hardware switches ( i . e . , finite state machines ) of size independent of the size of the network . </s> <pad> <pad> <pad>
Real Target String [18] 
		[['network'], ['processor'], ['abstraction'], ['space'], ['asynchronicity'], ['message'], ['size'], ['symmetry'], ['message', 'passing'], ['switch'], ['anonymity'], ['hardware'], ['model'], ['practical'], ['state', 'machine'], ['finite', 'state', 'machine'], ['management'], ['scheme']] 
Real Target Input:  
		[['<s>', 'network', '</s>'], ['<s>', 'processor', '</s>'], ['<s>', 'abstraction', '</s>'], ['<s>', 'space', '</s>'], ['<s>', 'asynchronicity', '</s>'], ['<s>', 'message', '</s>'], ['<s>', 'size', '</s>'], ['<s>', 'symmetry', '</s>'], ['<s>', 'message', 'passing', '</s>'], ['<s>', 'switch', '</s>'], ['<s>', 'anonymity', '</s>'], ['<s>', 'hardware', '</s>'], ['<s>', 'model', '</s>'], ['<s>', 'practical', '</s>'], ['<s>', 'state', 'machine', '</s>'], ['<s>', 'finite', 'state', 'machine', '</s>'], ['<s>', 'management', '</s>'], ['<s>', 'scheme', '</s>']] 
Real Target Copy:   
		[['network', '</s>'], ['processor', '</s>'], ['abstraction', '</s>'], ['space', '</s>'], ['asynchronicity', '</s>'], ['message', '</s>'], ['size', '</s>'], ['symmetry', '</s>'], ['message', 'passing', '</s>'], ['switch', '</s>'], ['anonymity', '</s>'], ['hardware', '</s>'], ['model', '</s>'], ['practical', '</s>'], ['state', 'machine', '</s>'], ['finite', 'state', 'machine', '</s>'], ['management', '</s>'], ['scheme', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=18/18
		[network]
		[processor]
		[abstraction]
		[space]
		[asynchronicity]
		[message]
		[size]
		[symmetry]
		[message passing]
		[switch]
		[anonymity]
		[hardware]
		[model]
		[practical]
		[state machine]
		[finite state machine]
		[management]
		[scheme]
oov_list:   
		[] 
[PREDICTION] #(valid)=121, #(present)=121, #(retained&present)=121, #(all)=121
		[4.6787]	*[simulation] 	 [93, 2] 
		[4.8119]	*[computer animation] 	 [157, 1747, 2] 
		[5.2675]	*[computational complexity] 	 [201, 211, 2] 
		[5.4801]	*[system buses] 	 [35, 6434, 2] 
		[5.5060]	*[scheduling] 	 [263, 2] 
		[5.5112]	*[reliability] 	 [542, 2] 
		[5.6191]	*[self stabilization] 	 [339, 3091, 2] 
		[5.7391]	*[multi agent systems] 	 [100, 465, 41, 2] 
		[5.8481]	*[self organization] 	 [339, 1252, 2] 
		[5.8902]	*[real time] 	 [102, 38, 2] 
		[6.0632]	*[fuzzy logic] 	 [83, 287, 2] 
		[6.1183]	*[genetic algorithms] 	 [340, 75, 2] 
		[6.2256]	*[cellular automata] 	 [940, 1064, 2] 
		[6.2772]	*[genetic algorithm] 	 [340, 37, 2] 
		[6.3072]	*[computer vision] 	 [157, 1051, 2] 
		[6.3271]	*[parallel] 	 [193, 2] 
		[6.3672]	*[power] 	 [107, 2] 
		[6.4713]	*[group] 	 [288, 2] 
		[6.4759]	*[computer graphics] 	 [157, 1249, 2] 
		[6.4957]	*[time series] 	 [38, 462, 2] 
		[6.5021]	*[dynamic] 	 [132, 2] 
		[6.5073]	*[artificial neural network] 	 [661, 184, 52, 2] 
		[6.5236]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[6.5239]	*[computational geometry] 	 [201, 937, 2] 
		[6.6009]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[6.6231]	*[system] 	 [35, 2] 
		[6.7007]	*[system identification] 	 [35, 590, 2] 
		[6.7328]	*[model] 	 [30, 2] [correct!]
		[6.7331]	*[multi agent system] 	 [100, 465, 35, 2] 
		[6.7595]	*[distributed systems] 	 [156, 41, 2] 
		[6.7708]	*[functional] 	 [439, 2] 
		[6.7980]	*[network] 	 [52, 2] [correct!]
		[6.8012]	*[computational efficiency] 	 [201, 220, 2] 
		[6.8066]	*[multi homing] 	 [100, 9450, 2] 
		[6.8184]	*[non natives] 	 [123, 16916, 2] 
		[6.8922]	*[distributed] 	 [156, 2] 
		[6.8959]	*[adaptive] 	 [194, 2] 
		[6.9845]	*[universal] 	 [1938, 2] 
		[7.0923]	*[open source] 	 [551, 336, 2] 
		[7.1049]	*[real time simulation] 	 [102, 38, 93, 2] 
		[7.1137]	*[artificial neural networks] 	 [661, 184, 62, 2] 
		[7.1219]	*[multi agent] 	 [100, 465, 2] 
		[7.1590]	*[<digit>] 	 [20, 2] 
		[7.1670]	*[self organized criticality] 	 [339, 2799, 9670, 2] 
		[7.1698]	*[open] 	 [551, 2] 
		[7.2589]	*[fuzzy logic controllers] 	 [83, 287, 2062, 2] 
		[7.3028]	*[power system] 	 [107, 35, 2] 
		[7.4108]	*[multi agent simulation] 	 [100, 465, 93, 2] 
		[7.4831]	*[product] 	 [359, 2] 
		[7.6407]	*[finite state machine] 	 [163, 151, 281, 2] [correct!]
		[7.6918]	*[finite state machines] 	 [163, 151, 791, 2] [correct!]
		[7.7690]	*[software] 	 [98, 2] 
		[7.7747]	*[universal audiovisual] 	 [1938, 7158, 2] 
		[7.8850]	*[real time systems] 	 [102, 38, 41, 2] 
		[7.9571]	*[real time animation] 	 [102, 38, 1747, 2] 
		[7.9962]	*[multi objective programming] 	 [100, 349, 160, 2] 
		[8.0962]	*[real time rendering] 	 [102, 38, 1243, 2] 
		[8.1417]	*[real time system] 	 [102, 38, 35, 2] 
		[8.1739]	*[multi objective optimisation] 	 [100, 349, 1629, 2] 
		[8.2834]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[8.2850]	*[graph] 	 [204, 2] 
		[8.3390]	*[self organizing map] 	 [339, 2029, 578, 2] 
		[8.3512]	*[computer] 	 [157, 2] 
		[8.3764]	*[group key cryptosystem] 	 [288, 261, 6665, 2] 
		[8.3953]	*[multi objective systems] 	 [100, 349, 41, 2] 
		[8.4124]	*[cellular] 	 [940, 2] 
		[8.4495]	*[multi objective] 	 [100, 349, 2] 
		[8.5743]	*[fuzzy] 	 [83, 2] 
		[8.6453]	*[multi user] 	 [100, 103, 2] 
		[8.6748]	*[fuzzy logic system] 	 [83, 287, 35, 2] 
		[8.7821]	*[self organizing maps] 	 [339, 2029, 979, 2] 
		[8.8059]	*[fuzzy logic programming] 	 [83, 287, 160, 2] 
		[8.8110]	*[real time processing] 	 [102, 38, 192, 2] 
		[8.9037]	*[real time implementation] 	 [102, 38, 186, 2] 
		[8.9973]	*[time series analysis] 	 [38, 462, 43, 2] 
		[9.0551]	*[group key distribution] 	 [288, 261, 207, 2] 
		[9.0761]	*[real time learning] 	 [102, 38, 77, 2] 
		[9.3045]	*[finite state] 	 [163, 151, 2] 
		[9.3527]	*[time] 	 [38, 2] 
		[9.4608]	*[computer aided] 	 [157, 2031, 2] 
		[9.5011]	*[universal audiovisual flow shop] 	 [1938, 7158, 178, 3280, 2] 
		[9.5151]	*[computational] 	 [201, 2] 
		[9.5278]	*[universal audiovisual flow] 	 [1938, 7158, 178, 2] 
		[9.8748]	*[self organizing] 	 [339, 2029, 2] 
		[9.9020]	*[multi objective evolutionary algorithm] 	 [100, 349, 611, 37, 2] 
		[9.9190]	*[genetic] 	 [340, 2] 
		[10.2456]	*[finite] 	 [163, 2] 
		[10.2797]	*[real] 	 [102, 2] 
		[10.4362]	*[self organized] 	 [339, 2799, 2] 
		[10.8632]	*[multi objective evolutionary] 	 [100, 349, 611, 2] 
		[10.9115]	*[multi objective evolutionary algorithms] 	 [100, 349, 611, 75, 2] 
		[11.2586]	*[group key] 	 [288, 261, 2] 
		[11.4463]	*[multi objective evolutionary systems] 	 [100, 349, 611, 41, 2] 
		[11.6730]	*[artificial neural] 	 [661, 184, 2] 
		[11.9920]	*[multi agent systems optimization] 	 [100, 465, 41, 95, 2] 
		[12.0245]	*[multi objective evolutionary programming] 	 [100, 349, 611, 160, 2] 
		[12.1230]	*[<digit> wolfram] 	 [20, 14081, 2] 
		[12.1618]	*[multi agent systems design] 	 [100, 465, 41, 50, 2] 
		[12.2446]	*[multi objective evolutionary optimization] 	 [100, 349, 611, 95, 2] 
		[12.2561]	*[multi agent system design] 	 [100, 465, 35, 50, 2] 
		[12.2940]	*[universal audiovisual flow realization] 	 [1938, 7158, 178, 3159, 2] 
		[12.6205]	*[finite state machine theory] 	 [163, 151, 281, 179, 2] 
		[12.7895]	*[artificial neural network analysis] 	 [661, 184, 52, 43, 2] 
		[12.7920]	*[computer aided design system] 	 [157, 2031, 50, 35, 2] 
		[12.8595]	*[multi objective optimization algorithm] 	 [100, 349, 95, 37, 2] 
		[12.8630]	*[multi objective evolutionary methods] 	 [100, 349, 611, 74, 2] 
		[12.8762]	*[multi objective evolutionary system] 	 [100, 349, 611, 35, 2] 
		[12.9101]	*[multi agent simulation optimization] 	 [100, 465, 93, 95, 2] 
		[12.9400]	*[multi objective optimization problem] 	 [100, 349, 95, 44, 2] 
		[12.9658]	*[multi agent systems system] 	 [100, 465, 41, 35, 2] 
		[12.9674]	*[finite state machine readable] 	 [163, 151, 281, 7220, 2] 
		[12.9700]	*[multi agent systems engineering] 	 [100, 465, 41, 416, 2] 
		[13.0153]	*[multi agent systems systems] 	 [100, 465, 41, 41, 2] 
		[13.0194]	*[finite state machine model] 	 [163, 151, 281, 30, 2] 
		[13.0312]	*[multi objective optimization algorithms] 	 [100, 349, 95, 75, 2] 
		[13.0798]	*[computer aided diagnosis system] 	 [157, 2031, 997, 35, 2] 
		[13.8007]	*[computer aided design and] 	 [157, 2031, 50, 8, 2] 
		[14.9255]	*[real time systems and] 	 [102, 38, 41, 8, 2] 
		[15.9191]	*[real time implementation and] 	 [102, 38, 186, 8, 2] 
		[16.7644]	*[multi agent systems and] 	 [100, 465, 41, 8, 2] 
		[16.9694]	*[multi agent systems of] 	 [100, 465, 41, 6, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.03253588516746412 , 0.04579630895420369 , 0.035920095165803016
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.019617224880382773 , 0.05384294068504595 , 0.027144624722653672
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.05444520428135065 , 0.08385966706263039 , 0.061556595700321674
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.04044087526330538 , 0.1254829234702026 , 0.0576587837022292
 =======================================================