[Source][135]: verifying real time properties of mos transistor circuits . a verification approach which allows the verification of functional and timing behavior of circuits at transistor level is presented . it is aimed at the verification of asynchronous interfaces and standard cell library modules . in contrast to other approaches , timing is explicitly considered , allowing one to verify timing dependent effects with a high degree of accuracy . to conveniently specify desired properties , a specification language based on linear quantized temporal logic ( qltl ) is provided . for an efficient verification , input constraints , necessary for a proper circuit functioning , are converted into input constraining automata , reducing the reachable state space and providing a model linearisation , necessary to prove linear qltl formulas by branching ctl model checking . 

Source Input: 
 <s> verifying real time properties of mos transistor circuits . a verification approach which allows the verification of functional and timing behavior of circuits at transistor level is presented . it is aimed at the verification of asynchronous interfaces and standard cell library modules . in contrast to other approaches , timing is explicitly considered , allowing one to verify timing dependent effects with a high degree of accuracy . to conveniently specify desired properties , a specification language based on linear quantized temporal logic ( <unk> ) is provided . for an efficient verification , input constraints , necessary for a proper circuit functioning , are converted into input constraining automata , reducing the reachable state space and providing a model linearisation , necessary to prove linear <unk> formulas by branching ctl model checking . </s>
Real Target String [26] 
		[['timing'], ['timing', 'dependent', 'effects'], ['specification', 'languages'], ['verification', 'approach'], ['reachable', 'state', 'space'], ['real', 'time', 'properties'], ['standard', 'cell', 'library', 'modules'], ['state', 'space', 'methods'], ['functional', 'behavior'], ['input', 'constraining', 'automata'], ['linear', 'quantized', 'temporal', 'logic'], ['model', 'linearisation'], ['timing', 'behavior'], ['circuit', 'layout', 'cad'], ['cellular', 'arrays'], ['temporal', 'logic'], ['circuit', 'analysis', 'computing'], ['branching', 'ctl', 'model', 'checking'], ['asynchronous', 'interfaces'], ['qltl'], ['mos', 'transistor', 'circuits'], ['mos', 'logic', 'circuits'], ['logic', 'partitioning'], ['logic', 'cad'], ['vlsi'], ['specification', 'language']] 
Real Target Input:  
		[['<s>', 'timing', '</s>'], ['<s>', 'timing', 'dependent', 'effects', '</s>'], ['<s>', 'specification', 'languages', '</s>'], ['<s>', 'verification', 'approach', '</s>'], ['<s>', 'reachable', 'state', 'space', '</s>'], ['<s>', 'real', 'time', 'properties', '</s>'], ['<s>', 'standard', 'cell', 'library', 'modules', '</s>'], ['<s>', 'state', 'space', 'methods', '</s>'], ['<s>', 'functional', 'behavior', '</s>'], ['<s>', 'input', 'constraining', 'automata', '</s>'], ['<s>', 'linear', 'quantized', 'temporal', 'logic', '</s>'], ['<s>', 'model', 'linearisation', '</s>'], ['<s>', 'timing', 'behavior', '</s>'], ['<s>', 'circuit', 'layout', 'cad', '</s>'], ['<s>', 'cellular', 'arrays', '</s>'], ['<s>', 'temporal', 'logic', '</s>'], ['<s>', 'circuit', 'analysis', 'computing', '</s>'], ['<s>', 'branching', 'ctl', 'model', 'checking', '</s>'], ['<s>', 'asynchronous', 'interfaces', '</s>'], ['<s>', '<unk>', '</s>'], ['<s>', 'mos', 'transistor', 'circuits', '</s>'], ['<s>', 'mos', 'logic', 'circuits', '</s>'], ['<s>', 'logic', 'partitioning', '</s>'], ['<s>', 'logic', 'cad', '</s>'], ['<s>', 'vlsi', '</s>'], ['<s>', 'specification', 'language', '</s>']] 
Real Target Copy:   
		[['timing', '</s>'], ['timing', 'dependent', 'effects', '</s>'], ['specification', 'languages', '</s>'], ['verification', 'approach', '</s>'], ['reachable', 'state', 'space', '</s>'], ['real', 'time', 'properties', '</s>'], ['standard', 'cell', 'library', 'modules', '</s>'], ['state', 'space', 'methods', '</s>'], ['functional', 'behavior', '</s>'], ['input', 'constraining', 'automata', '</s>'], ['linear', 'quantized', 'temporal', 'logic', '</s>'], ['model', 'linearisation', '</s>'], ['timing', 'behavior', '</s>'], ['circuit', 'layout', 'cad', '</s>'], ['cellular', 'arrays', '</s>'], ['temporal', 'logic', '</s>'], ['circuit', 'analysis', 'computing', '</s>'], ['branching', 'ctl', 'model', 'checking', '</s>'], ['asynchronous', 'interfaces', '</s>'], ['qltl', '</s>'], ['mos', 'transistor', 'circuits', '</s>'], ['mos', 'logic', 'circuits', '</s>'], ['logic', 'partitioning', '</s>'], ['logic', 'cad', '</s>'], ['vlsi', '</s>'], ['specification', 'language', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=16/26
		[timing]
		[timing dependent effects]
		[specification languages]
		[verification approach]
		[reachable state space]
		[real time properties]
		[standard cell library modules]
		state space methods
		functional behavior
		[input constraining automata]
		[linear quantized temporal logic]
		[model linearisation]
		[timing behavior]
		circuit layout cad
		cellular arrays
		[temporal logic]
		circuit analysis computing
		[branching ctl model checking]
		[asynchronous interfaces]
		[qltl]
		[mos transistor circuits]
		mos logic circuits
		logic partitioning
		logic cad
		vlsi
		specification language
oov_list:   
		['qltl'] 
[PREDICTION] #(valid)=118, #(present)=118, #(retained&present)=118, #(all)=118
		[3.7676]	*[simulation] 	 [93, 2] 
		[4.3818]	*[performance] 	 [45, 2] 
		[4.4011]	*[method] 	 [34, 2] 
		[4.5809]	*[design] 	 [50, 2] 
		[4.7062]	*[parallel] 	 [193, 2] 
		[4.7573]	*[reliability] 	 [542, 2] 
		[4.8930]	*[paper] 	 [28, 2] 
		[4.9163]	*[implementation] 	 [186, 2] 
		[4.9518]	*[optimization] 	 [95, 2] 
		[5.0441]	*[process] 	 [73, 2] 
		[5.0883]	*[fpga] 	 [1573, 2] 
		[5.2013]	*[measurement] 	 [588, 2] 
		[5.2280]	*[analysis] 	 [43, 2] 
		[5.2326]	*[algorithm] 	 [37, 2] 
		[5.3097]	*[vectorization] 	 [5877, 2] 
		[5.3861]	*[embedded systems] 	 [584, 41, 2] 
		[5.4023]	*[systems] 	 [41, 2] 
		[5.4538]	*[performance evaluation] 	 [45, 172, 2] 
		[5.4852]	*[computer animation] 	 [157, 1747, 2] 
		[5.5309]	*[performance analysis] 	 [45, 43, 2] 
		[5.6340]	*[power] 	 [107, 2] 
		[5.7803]	*[software engineering] 	 [98, 416, 2] 
		[5.8147]	*[functional] 	 [439, 2] 
		[5.8704]	*[parallel computing] 	 [193, 188, 2] 
		[5.9097]	*[model] 	 [30, 2] 
		[5.9741]	*[multi agent systems] 	 [100, 465, 41, 2] 
		[6.0867]	*[test] 	 [164, 2] 
		[6.0955]	*[functional programming] 	 [439, 160, 2] 
		[6.1666]	*[parallel processing] 	 [193, 192, 2] 
		[6.2382]	*[model checking] 	 [30, 1069, 2] 
		[6.3090]	*[parallel algorithms] 	 [193, 75, 2] 
		[6.3484]	*[real time] 	 [102, 38, 2] 
		[6.3748]	*[dynamic] 	 [132, 2] 
		[6.4515]	*[performance optimization] 	 [45, 95, 2] 
		[6.4780]	*[parallel programming] 	 [193, 160, 2] 
		[6.5879]	*[software] 	 [98, 2] 
		[6.6522]	*[distributed systems] 	 [156, 41, 2] 
		[6.7611]	*[parallel algorithm] 	 [193, 37, 2] 
		[6.7809]	*[parallel implementation] 	 [193, 186, 2] 
		[6.8256]	*[parallel systems] 	 [193, 41, 2] 
		[6.8289]	*[runtime] 	 [1403, 2] 
		[6.8366]	*[dynamic programming] 	 [132, 160, 2] 
		[6.8799]	*[functional languages] 	 [439, 598, 2] 
		[6.9076]	*[power dissipation] 	 [107, 2808, 2] 
		[6.9087]	*[performance measurement] 	 [45, 588, 2] 
		[6.9107]	*[model driven engineering] 	 [30, 655, 416, 2] 
		[6.9171]	*[computer graphics] 	 [157, 1249, 2] 
		[6.9615]	*[runtime system] 	 [1403, 35, 2] 
		[6.9813]	*[distributed] 	 [156, 2] 
		[7.0119]	*[object] 	 [210, 2] 
		[7.0775]	*[power system] 	 [107, 35, 2] 
		[7.0819]	*[adaptive] 	 [194, 2] 
		[7.0875]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[7.0907]	*[data] 	 [29, 2] 
		[7.2008]	*[real time systems] 	 [102, 38, 41, 2] 
		[7.2825]	*[real time simulation] 	 [102, 38, 93, 2] 
		[7.3960]	*[system] 	 [35, 2] 
		[7.5137]	*[multi agent system] 	 [100, 465, 35, 2] 
		[7.5421]	*[embedded] 	 [584, 2] 
		[7.6058]	*[object oriented design] 	 [210, 435, 50, 2] 
		[7.7032]	*[real time animation] 	 [102, 38, 1747, 2] 
		[7.7110]	*[analysis of algorithms] 	 [43, 6, 75, 2] 
		[7.7665]	*[multi agent] 	 [100, 465, 2] 
		[7.7929]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[7.8107]	*[object oriented] 	 [210, 435, 2] 
		[8.0753]	*[real time system] 	 [102, 38, 35, 2] 
		[8.2565]	*[multi agent simulation] 	 [100, 465, 93, 2] 
		[8.3010]	*[multi objective programming] 	 [100, 349, 160, 2] 
		[8.3572]	*[real time implementation] 	 [102, 38, 186, 2] 
		[8.4153]	*[model based testing] 	 [30, 26, 475, 2] 
		[8.5040]	*[model based design] 	 [30, 26, 50, 2] 
		[8.5490]	*[real time processing] 	 [102, 38, 192, 2] 
		[8.5539]	*[real time rendering] 	 [102, 38, 1243, 2] 
		[8.7235]	*[analysis of design] 	 [43, 6, 50, 2] 
		[8.7509]	*[model driven development] 	 [30, 655, 155, 2] 
		[8.8374]	*[object oriented programming] 	 [210, 435, 160, 2] 
		[8.8403]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[9.0398]	*[real time graphics] 	 [102, 38, 1249, 2] 
		[9.1692]	*[real time java] 	 [102, 38, 1075, 2] 
		[9.1707]	*[real time applications] 	 [102, 38, 92, 2] 
		[9.1711]	*[real time learning] 	 [102, 38, 77, 2] 
		[9.2981]	*[method of computation] 	 [34, 6, 313, 2] 
		[9.3130]	*[computer] 	 [157, 2] 
		[9.3688]	*[power system variability] 	 [107, 35, 1618, 2] 
		[9.4568]	*[model driven design] 	 [30, 655, 50, 2] 
		[9.5194]	*[real time design] 	 [102, 38, 50, 2] 
		[10.0279]	*[model based] 	 [30, 26, 2] 
		[10.0552]	*[multi objective] 	 [100, 349, 2] 
		[10.1756]	*[model driven] 	 [30, 655, 2] 
		[10.4778]	*[computer aided] 	 [157, 2031, 2] 
		[11.2680]	*[real] 	 [102, 2] 
		[11.4208]	*[performance evaluation and design] 	 [45, 172, 8, 50, 2] 
		[11.4312]	*[performance evaluation and analysis] 	 [45, 172, 8, 43, 2] 
		[11.6523]	*[analysis of] 	 [43, 6, 2] 
		[11.7699]	*[method of the loop] 	 [34, 6, 4, 977, 2] 
		[12.4210]	*[performance evaluation and evaluation] 	 [45, 172, 8, 172, 2] 
		[12.5837]	*[multi agent systems optimization] 	 [100, 465, 41, 95, 2] 
		[12.5926]	*[performance evaluation and verification] 	 [45, 172, 8, 729, 2] 
		[12.6767]	*[performance evaluation and discovery] 	 [45, 172, 8, 969, 2] 
		[12.7266]	*[multi agent systems design] 	 [100, 465, 41, 50, 2] 
		[12.7275]	*[model driven engineering design] 	 [30, 655, 416, 50, 2] 
		[12.7767]	*[model based design engineering] 	 [30, 26, 50, 416, 2] 
		[12.8409]	*[model based design optimization] 	 [30, 26, 50, 95, 2] 
		[12.9073]	*[method of the modules] 	 [34, 6, 4, 1679, 2] 
		[12.9324]	*[performance evaluation and control] 	 [45, 172, 8, 66, 2] 
		[13.0250]	*[performance evaluation and management] 	 [45, 172, 8, 144, 2] 
		[13.7757]	*[performance evaluation and] 	 [45, 172, 8, 2] 
		[14.5108]	*[method of the defect] 	 [34, 6, 4, 2716, 2] 
		[14.8299]	*[real time systems and] 	 [102, 38, 41, 8, 2] 
		[14.9337]	*[computer aided design and] 	 [157, 2031, 50, 8, 2] 
		[15.8386]	*[real time implementation and] 	 [102, 38, 186, 8, 2] 
		[15.9652]	*[model based design and] 	 [30, 26, 50, 8, 2] 
		[15.9785]	*[model driven development and] 	 [30, 655, 155, 8, 2] 
		[16.0632]	*[real time design and] 	 [102, 38, 50, 8, 2] 
		[16.5481]	*[power system variability and] 	 [107, 35, 1618, 8, 2] 
		[16.8005]	*[method of the order] 	 [34, 6, 4, 80, 2] 
		[17.7034]	*[multi agent systems and] 	 [100, 465, 41, 8, 2] 
		[17.8590]	*[real time java and] 	 [102, 38, 1075, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.02565217391304348 , 0.03647379190857452 , 0.026930803012291102
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.017391304347826084 , 0.04592416440242528 , 0.0225030451868366
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.048934437920865805 , 0.07499463908262378 , 0.05335027817747948
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.03924465183492588 , 0.11787453313915451 , 0.05376166215055872
 =======================================================