[Source][168]: branch less , cut more and minimize the number of late equal length jobs on identical machines . a simple greedy type solution for a discrete optimization problem does not guarantee the optimality if the problem is sufficiently complicated . dynamic programming is then a commonly used method , and a direct combinatorial algorithm is its reasonable alternative . here we propose such an algorithm with some specific features , called branch less and cut more , abbreviated blesscmore . a blesscmore algorithm , like a branch and bound algorithm uses a solution tree whereas the branching and cutting criteria are based on the analysis of the so called behavior alternatives . our o ( n ( <digit> ) log n ) blesscmore algorithm solves an earlier open problem of scheduling n equal length jobs with release times and due dates on a group of identical machines to minimize the number of late jobs . ( c ) <digit> elsevier b . v . all rights reserved . 

Source Input: 
 <s> branch less , cut more and minimize the number of late equal length jobs on identical machines . a simple greedy type solution for a discrete optimization problem does not guarantee the optimality if the problem is sufficiently complicated . dynamic programming is then a commonly used method , and a direct combinatorial algorithm is its reasonable alternative . here we propose such an algorithm with some specific features , called branch less and cut more , abbreviated <unk> . a <unk> algorithm , like a branch and bound algorithm uses a solution tree whereas the branching and cutting criteria are based on the analysis of the so called behavior alternatives . our o ( n ( <digit> ) log n ) <unk> algorithm solves an earlier open problem of scheduling n equal length jobs with release times and due dates on a group of identical machines to minimize the number of late jobs . ( c ) <digit> elsevier b . v . all rights reserved . </s> <pad> <pad> <pad>
Real Target String [3] 
		[['polynomial', 'time', 'algorithm'], ['solution', 'tree'], ['scheduling', 'identical', 'machines']] 
Real Target Input:  
		[['<s>', 'polynomial', 'time', 'algorithm', '</s>'], ['<s>', 'solution', 'tree', '</s>'], ['<s>', 'scheduling', 'identical', 'machines', '</s>']] 
Real Target Copy:   
		[['polynomial', 'time', 'algorithm', '</s>'], ['solution', 'tree', '</s>'], ['scheduling', 'identical', 'machines', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=1/3
		polynomial time algorithm
		[solution tree]
		scheduling identical machines
oov_list:   
		['blesscmore'] 
[PREDICTION] #(valid)=115, #(present)=115, #(retained&present)=115, #(all)=115
		[3.7195]	*[genetic algorithm] 	 [340, 37, 2] 
		[4.2507]	*[genetic algorithms] 	 [340, 75, 2] 
		[4.7599]	*[neural network] 	 [184, 52, 2] 
		[4.7796]	*[markov random fields] 	 [752, 276, 842, 2] 
		[4.8838]	*[neural networks] 	 [184, 62, 2] 
		[5.2917]	*[optimization] 	 [95, 2] 
		[5.3527]	*[tracking] 	 [480, 2] 
		[5.4342]	*[evolutionary algorithm] 	 [611, 37, 2] 
		[5.5195]	*[classification] 	 [181, 2] 
		[5.5640]	*[particle swarm optimization] 	 [689, 1313, 95, 2] 
		[5.5648]	*[prediction] 	 [380, 2] 
		[5.6011]	*[principal component analysis] 	 [1288, 414, 43, 2] 
		[5.6463]	*[evolutionary algorithms] 	 [611, 75, 2] 
		[5.6560]	*[scheduling] 	 [263, 2] 
		[5.7304]	*[markov chain monte carlo] 	 [752, 596, 1225, 1226, 2] 
		[5.7555]	*[uncertainty] 	 [685, 2] 
		[5.8335]	*[fuzzy logic] 	 [83, 287, 2] 
		[5.9185]	*[missing data] 	 [1923, 29, 2] 
		[5.9620]	*[graph recoloring] 	 [204, 14467, 2] 
		[5.9951]	*[decision support system] 	 [173, 130, 35, 2] 
		[6.1107]	*[decision making] 	 [173, 428, 2] 
		[6.1768]	*[markov chain] 	 [752, 596, 2] 
		[6.2119]	*[gibbs sampling] 	 [4924, 711, 2] 
		[6.2921]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[6.4151]	*[approximation algorithms] 	 [320, 75, 2] 
		[6.4523]	*[gibbs sampler] 	 [4924, 7571, 2] 
		[6.5209]	*[artificial neural network] 	 [661, 184, 52, 2] 
		[6.5415]	*[graph algorithms] 	 [204, 75, 2] 
		[6.6355]	*[computational complexity] 	 [201, 211, 2] 
		[6.6445]	*[graph coloring] 	 [204, 2300, 2] 
		[6.7071]	*[fuzzy logic controllers] 	 [83, 287, 2062, 2] 
		[6.7136]	*[fuzzy set] 	 [83, 79, 2] 
		[6.7840]	*[artificial neural networks] 	 [661, 184, 62, 2] 
		[6.7934]	*[adaptive control] 	 [194, 66, 2] 
		[6.9231]	*[adaptive meshing] 	 [194, 6276, 2] 
		[7.0027]	*[particle swarm optimizers] 	 [689, 1313, 10752, 2] 
		[7.0123]	*[approximation] 	 [320, 2] 
		[7.0382]	*[bayesian networks] 	 [824, 62, 2] 
		[7.1515]	*[decision tree] 	 [173, 296, 2] 
		[7.1620]	*[linear discriminant analysis] 	 [109, 1785, 43, 2] 
		[7.4429]	*[markov random field] 	 [752, 276, 236, 2] 
		[7.5837]	*[multi agent systems] 	 [100, 465, 41, 2] 
		[7.6105]	*[decision support systems] 	 [173, 130, 41, 2] 
		[7.6347]	*[artificial bee colony optimization] 	 [661, 5922, 2614, 95, 2] 
		[7.6706]	*[adaptive] 	 [194, 2] 
		[7.9935]	*[graph] 	 [204, 2] 
		[8.0456]	*[decision support] 	 [173, 130, 2] 
		[8.0490]	*[particle swarm optimisation] 	 [689, 1313, 1629, 2] 
		[8.2369]	*[multi agent system] 	 [100, 465, 35, 2] 
		[8.2949]	*[fuzzy logic system] 	 [83, 287, 35, 2] 
		[8.4077]	*[multi objective optimisation] 	 [100, 349, 1629, 2] 
		[8.4347]	*[missing] 	 [1923, 2] 
		[8.5151]	*[decision] 	 [173, 2] 
		[8.5453]	*[artificial bee colony algorithm] 	 [661, 5922, 2614, 37, 2] 
		[8.6778]	*[spatiotemporal] 	 [4235, 2] 
		[8.7341]	*[markov] 	 [752, 2] 
		[8.9135]	*[evolutionary] 	 [611, 2] 
		[8.9220]	*[fuzzy logic systems] 	 [83, 287, 41, 2] 
		[8.9271]	*[bayesian] 	 [824, 2] 
		[8.9620]	*[multi agent simulation] 	 [100, 465, 93, 2] 
		[9.0110]	*[neural] 	 [184, 2] 
		[9.0188]	*[fuzzy] 	 [83, 2] 
		[9.0208]	*[stochastic] 	 [529, 2] 
		[9.0255]	*[fuzzy logic programming] 	 [83, 287, 160, 2] 
		[9.1062]	*[neural network analysis] 	 [184, 52, 43, 2] 
		[9.1633]	*[genetic] 	 [340, 2] 
		[9.1955]	*[markov decision process] 	 [752, 173, 73, 2] 
		[9.2278]	*[markov decision] 	 [752, 173, 2] 
		[9.2424]	*[multi agent] 	 [100, 465, 2] 
		[9.2827]	*[probabilistic] 	 [716, 2] 
		[9.2919]	*[multi objective evolutionary algorithm] 	 [100, 349, 611, 37, 2] 
		[9.4058]	*[neural network models] 	 [184, 52, 69, 2] 
		[9.4535]	*[markov decision processes] 	 [752, 173, 338, 2] 
		[9.4643]	*[soft] 	 [1048, 2] 
		[9.4711]	*[markov decision support system] 	 [752, 173, 130, 35, 2] 
		[9.5265]	*[neural network model] 	 [184, 52, 30, 2] 
		[9.5729]	*[neural network estimation] 	 [184, 52, 223, 2] 
		[9.6278]	*[fuzzy logic controller] 	 [83, 287, 767, 2] 
		[9.6571]	*[gibbs] 	 [4924, 2] 
		[9.8955]	*[artificial bee colony system] 	 [661, 5922, 2614, 35, 2] 
		[9.9786]	*[linear] 	 [109, 2] 
		[10.0050]	*[financial] 	 [2020, 2] 
		[10.0973]	*[artificial bee colony] 	 [661, 5922, 2614, 2] 
		[10.2999]	*[particle] 	 [689, 2] 
		[10.3739]	*[particle swarm] 	 [689, 1313, 2] 
		[10.4025]	*[principal component] 	 [1288, 414, 2] 
		[10.4546]	*[linear discriminant] 	 [109, 1785, 2] 
		[10.4582]	*[artificial bee colony programming] 	 [661, 5922, 2614, 160, 2] 
		[10.4759]	*[multi objective evolutionary algorithms] 	 [100, 349, 611, 75, 2] 
		[10.5848]	*[artificial bee colony optimisation] 	 [661, 5922, 2614, 1629, 2] 
		[10.6351]	*[multi objective] 	 [100, 349, 2] 
		[10.6427]	*[artificial bee colony systems] 	 [661, 5922, 2614, 41, 2] 
		[10.6984]	*[markov decision support systems] 	 [752, 173, 130, 41, 2] 
		[10.9581]	*[markov random field model] 	 [752, 276, 236, 30, 2] 
		[11.1506]	*[particle swarm optimization algorithm] 	 [689, 1313, 95, 37, 2] 
		[11.1569]	*[artificial bee colony learning] 	 [661, 5922, 2614, 77, 2] 
		[11.4119]	*[markov decision support] 	 [752, 173, 130, 2] 
		[11.4629]	*[multi objective evolutionary optimization] 	 [100, 349, 611, 95, 2] 
		[11.5199]	*[artificial bee colony networks] 	 [661, 5922, 2614, 62, 2] 
		[11.5508]	*[markov random] 	 [752, 276, 2] 
		[11.5511]	*[artificial bee colony optimizers] 	 [661, 5922, 2614, 10752, 2] 
		[11.5973]	*[artificial bee colony robot] 	 [661, 5922, 2614, 629, 2] 
		[11.6359]	*[markov random field problem] 	 [752, 276, 236, 44, 2] 
		[11.6789]	*[multi objective optimization algorithm] 	 [100, 349, 95, 37, 2] 
		[11.7923]	*[multi objective optimization algorithms] 	 [100, 349, 95, 75, 2] 
		[11.8451]	*[particle swarm optimization algorithms] 	 [689, 1313, 95, 75, 2] 
		[11.8812]	*[artificial bee colony algorithms] 	 [661, 5922, 2614, 75, 2] 
		[11.9010]	*[artificial neural] 	 [661, 184, 2] 
		[11.9197]	*[markov random field systems] 	 [752, 276, 236, 41, 2] 
		[12.0622]	*[artificial bee colony classifier] 	 [661, 5922, 2614, 879, 2] 
		[12.1592]	*[multi objective evolutionary] 	 [100, 349, 611, 2] 
		[13.6144]	*[artificial bee] 	 [661, 5922, 2] 
		[14.6559]	*[markov random fields and] 	 [752, 276, 842, 8, 2] 
		[17.4917]	*[fuzzy logic of neural] 	 [83, 287, 6, 184, 2] 
		[17.9623]	*[artificial bee colony neural] 	 [661, 5922, 2614, 184, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.02971428571428571 , 0.03993197278911565 , 0.03185577261207513
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.01885714285714286 , 0.04954195011337869 , 0.02555292208896809
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.052451701079096115 , 0.07923970389872992 , 0.05813634422624917
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.03908843571799142 , 0.12020373248681042 , 0.05505450304864685
 =======================================================