[Source][77]: factors for a successful adoption of budgetary transparency innovations a questionnaire report of an open government initiative in mexico . this study explores multiple success factors for open government adoption . the study examines an open government adoption for budgeting in mexico . a questionnaire was applied across different staff members in government . various factors of collaboration , knowledge , and trust were highly influential . derived from these results some practical advises are presented . 

Source Input: 
 <s> factors for a successful adoption of budgetary transparency innovations a questionnaire report of an open government initiative in mexico . this study explores multiple success factors for open government adoption . the study examines an open government adoption for budgeting in mexico . a questionnaire was applied across different staff members in government . various factors of collaboration , knowledge , and trust were highly influential . derived from these results some practical advises are presented . </s> <pad> <pad> <pad>
Real Target String [4] 
		[['open', 'government'], ['factors', 'of', 'adoption'], ['questionnaire'], ['budgeting']] 
Real Target Input:  
		[['<s>', 'open', 'government', '</s>'], ['<s>', 'factors', 'of', 'adoption', '</s>'], ['<s>', 'questionnaire', '</s>'], ['<s>', 'budgeting', '</s>']] 
Real Target Copy:   
		[['open', 'government', '</s>'], ['factors', 'of', 'adoption', '</s>'], ['questionnaire', '</s>'], ['budgeting', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=3/4
		[open government]
		factors of adoption
		[questionnaire]
		[budgeting]
oov_list:   
		[] 
[PREDICTION] #(valid)=121, #(present)=121, #(retained&present)=121, #(all)=121
		[4.6861]	*[computer animation] 	 [157, 1747, 2] 
		[4.9743]	*[collaboration] 	 [1201, 2] 
		[5.0656]	*[e learning] 	 [129, 77, 2] 
		[5.1233]	*[internet] 	 [401, 2] 
		[5.1570]	*[research] 	 [119, 2] 
		[5.2960]	*[usability] 	 [1552, 2] 
		[5.5007]	*[funding] 	 [6131, 2] 
		[5.5244]	*[technology adoption] 	 [235, 1406, 2] 
		[5.6251]	*[iso <digit>] 	 [3454, 20, 2] 
		[5.7573]	*[e government] 	 [129, 2030, 2] 
		[5.9328]	*[computer science] 	 [157, 770, 2] 
		[5.9389]	*[nursing] 	 [2848, 2] 
		[5.9491]	*[interaction] 	 [253, 2] 
		[5.9882]	*[systematic literature review] 	 [1373, 473, 868, 2] 
		[6.0611]	*[social media] 	 [273, 692, 2] 
		[6.0811]	*[electronic commerce] 	 [786, 1931, 2] 
		[6.1333]	*[virtual environment] 	 [312, 227, 2] 
		[6.1342]	*[virtual laboratory] 	 [312, 1905, 2] 
		[6.1544]	*[human computer interaction] 	 [252, 157, 253, 2] 
		[6.2455]	*[information integration] 	 [47, 437, 2] 
		[6.2704]	*[software engineering] 	 [98, 416, 2] 
		[6.2719]	*[patent infringement] 	 [2658, 16100, 2] 
		[6.2721]	*[web categorisation] 	 [124, 21278, 2] 
		[6.3022]	*[computer mediated communication] 	 [157, 2197, 180, 2] 
		[6.3478]	*[knowledge acquisition] 	 [134, 1575, 2] 
		[6.3623]	*[knowledge management] 	 [134, 144, 2] 
		[6.3692]	*[business] 	 [541, 2] 
		[6.4172]	*[information retrieval] 	 [47, 323, 2] 
		[6.4354]	*[computer learning] 	 [157, 77, 2] 
		[6.4511]	*[virtual reality] 	 [312, 1920, 2] 
		[6.4725]	*[interdisciplinary] 	 [4951, 2] 
		[6.4866]	*[collaborative learning] 	 [682, 77, 2] 
		[6.5229]	*[information technology] 	 [47, 235, 2] 
		[6.5665]	*[innovation] 	 [2120, 2] 
		[6.6142]	*[computer science curriculum] 	 [157, 770, 2911, 2] 
		[6.6392]	*[electronic health record] 	 [786, 763, 2181, 2] 
		[6.6794]	*[decision support system] 	 [173, 130, 35, 2] 
		[6.7487]	*[social tagging] 	 [273, 3409, 2] 
		[6.7714]	*[information seeking] 	 [47, 2870, 2] 
		[6.7737]	*[computer supported cooperative work] 	 [157, 1250, 1273, 138, 2] 
		[6.8251]	*[multimedia] 	 [654, 2] 
		[6.8398]	*[public key cryptosystem] 	 [1036, 261, 6665, 2] 
		[6.8534]	*[social network] 	 [273, 52, 2] 
		[7.0637]	*[information] 	 [47, 2] 
		[7.0638]	*[nurse] 	 [6594, 2] 
		[7.1019]	*[technology] 	 [235, 2] 
		[7.1338]	*[knowledge] 	 [134, 2] 
		[7.1458]	*[business process re engineering] 	 [541, 73, 1222, 416, 2] 
		[7.3270]	*[user] 	 [103, 2] 
		[7.4525]	*[business process modeling] 	 [541, 73, 148, 2] 
		[7.4779]	*[decision support systems] 	 [173, 130, 41, 2] 
		[7.5857]	*[decision support] 	 [173, 130, 2] 
		[7.6181]	*[social media sites] 	 [273, 692, 1290, 2] 
		[7.7097]	*[computer] 	 [157, 2] 
		[7.8324]	*[social] 	 [273, 2] 
		[8.0374]	*[iso <digit> standard] 	 [3454, 20, 316, 2] 
		[8.0600]	*[software] 	 [98, 2] 
		[8.1338]	*[patent] 	 [2658, 2] 
		[8.1359]	*[public] 	 [1036, 2] 
		[8.1442]	*[computer supported collaborative work] 	 [157, 1250, 682, 138, 2] 
		[8.1876]	*[social network sites] 	 [273, 52, 1290, 2] 
		[8.2088]	*[business process] 	 [541, 73, 2] 
		[8.2913]	*[computer supported collaborative learning] 	 [157, 1250, 682, 77, 2] 
		[8.2990]	*[computer science education] 	 [157, 770, 995, 2] 
		[8.3147]	*[electronic] 	 [786, 2] 
		[8.4763]	*[business process modelling] 	 [541, 73, 516, 2] 
		[8.5123]	*[information technology curriculum] 	 [47, 235, 2911, 2] 
		[8.5500]	*[collaborative] 	 [682, 2] 
		[8.5638]	*[business process system] 	 [541, 73, 35, 2] 
		[8.6317]	*[human] 	 [252, 2] 
		[8.7120]	*[e] 	 [129, 2] 
		[8.8835]	*[web] 	 [124, 2] 
		[9.0571]	*[public key cryptosystems] 	 [1036, 261, 9012, 2] 
		[9.1032]	*[decision] 	 [173, 2] 
		[9.1204]	*[information technology benefit realization] 	 [47, 235, 1700, 3159, 2] 
		[9.1299]	*[business process models] 	 [541, 73, 69, 2] 
		[9.1482]	*[business process systems] 	 [541, 73, 41, 2] 
		[9.1981]	*[e government curriculum] 	 [129, 2030, 2911, 2] 
		[9.2353]	*[electronic health records] 	 [786, 763, 1754, 2] 
		[9.2608]	*[interactive] 	 [536, 2] 
		[9.2880]	*[business process management] 	 [541, 73, 144, 2] 
		[9.3391]	*[social tagging sites] 	 [273, 3409, 1290, 2] 
		[9.3610]	*[iso] 	 [3454, 2] 
		[9.4106]	*[information technology education] 	 [47, 235, 995, 2] 
		[9.4448]	*[public key cryptography] 	 [1036, 261, 2154, 2] 
		[9.4676]	*[business process simulation] 	 [541, 73, 93, 2] 
		[9.4727]	*[computer mediated systems] 	 [157, 2197, 41, 2] 
		[9.5718]	*[electronic health] 	 [786, 763, 2] 
		[9.5753]	*[business process design] 	 [541, 73, 50, 2] 
		[10.3117]	*[human computer] 	 [252, 157, 2] 
		[10.5177]	*[computer supported collaborative technology] 	 [157, 1250, 682, 235, 2] 
		[10.7268]	*[computer mediated] 	 [157, 2197, 2] 
		[10.8114]	*[business process re outsourcing] 	 [541, 73, 1222, 3322, 2] 
		[11.1399]	*[business process re development] 	 [541, 73, 1222, 155, 2] 
		[11.1657]	*[electronic health record systems] 	 [786, 763, 2181, 41, 2] 
		[11.4084]	*[computer supported cooperative technology] 	 [157, 1250, 1273, 235, 2] 
		[11.4762]	*[computer supported cooperative systems] 	 [157, 1250, 1273, 41, 2] 
		[11.7718]	*[computer supported collaborative games] 	 [157, 1250, 682, 1065, 2] 
		[11.8226]	*[computer supported cooperative logics] 	 [157, 1250, 1273, 2820, 2] 
		[11.8702]	*[computer supported collaborative systems] 	 [157, 1250, 682, 41, 2] 
		[11.9640]	*[computer supported cooperative communications] 	 [157, 1250, 1273, 1294, 2] 
		[12.1150]	*[electronic health record system] 	 [786, 763, 2181, 35, 2] 
		[12.1353]	*[computer supported cooperative learning] 	 [157, 1250, 1273, 77, 2] 
		[12.2975]	*[public key] 	 [1036, 261, 2] 
		[12.3344]	*[computer supported collaborative design] 	 [157, 1250, 682, 50, 2] 
		[12.3909]	*[iso <digit> standard standard] 	 [3454, 20, 316, 316, 2] 
		[12.5027]	*[computer supported collaborative behavior] 	 [157, 1250, 682, 298, 2] 
		[12.5511]	*[systematic literature] 	 [1373, 473, 2] 
		[12.6417]	*[computer supported collaborative] 	 [157, 1250, 682, 2] 
		[12.6657]	*[information technology benefit] 	 [47, 235, 1700, 2] 
		[12.7498]	*[computer supported cooperative logic] 	 [157, 1250, 1273, 287, 2] 
		[13.0722]	*[business process re project] 	 [541, 73, 1222, 540, 2] 
		[13.2131]	*[computer science curriculum and] 	 [157, 770, 2911, 8, 2] 
		[13.7867]	*[business process re] 	 [541, 73, 1222, 2] 
		[14.4919]	*[systematic literature review and] 	 [1373, 473, 868, 8, 2] 
		[15.1094]	*[e government curriculum and] 	 [129, 2030, 2911, 8, 2] 
		[15.7928]	*[computer supported cooperative collaborative] 	 [157, 1250, 1273, 682, 2] 
		[16.3340]	*[information technology curriculum and] 	 [47, 235, 2911, 8, 2] 
		[16.4714]	*[business process modeling and] 	 [541, 73, 148, 8, 2] 
		[16.8891]	*[social media sites and] 	 [273, 692, 1290, 8, 2] 
		[16.9048]	*[public key cryptosystem and] 	 [1036, 261, 6665, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.024516129032258062 , 0.03544546850998464 , 0.027468504508352712
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.015806451612903227 , 0.04388120839733743 , 0.021755770445473832
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.044236047436152726 , 0.06881235222347916 , 0.050480843540771904
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.03333333432674408 , 0.11111111442248027 , 0.05128205281037551
 --- total precision, recall, fscore: 0.035755504684582835 , 0.11027950124731438 , 0.050928855346598315
 =======================================================