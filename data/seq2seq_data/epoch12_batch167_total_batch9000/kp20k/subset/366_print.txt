[Source][65]: programming the world with sun spots . we describe the sun1 small programmable object technology , or sun spot . the sun spot is a small wireless computing platform that runs java1 directly , with no operating system . the system comes with an on board set of sensors , and i o pins for easy connection to external devices , and supporting software . 

Source Input: 
 <s> programming the world with sun spots . we describe the <unk> small programmable object technology , or sun spot . the sun spot is a small wireless computing platform that runs <unk> directly , with no operating system . the system comes with an on board set of sensors , and i o pins for easy connection to external devices , and supporting software . </s> <pad> <pad> <pad>
Real Target String [5] 
		[['sensors', 'sensor', 'networks'], ['java'], ['mesh', 'networks'], ['network', 'management'], ['distributed', 'programming']] 
Real Target Input:  
		[['<s>', 'sensors', 'sensor', 'networks', '</s>'], ['<s>', 'java', '</s>'], ['<s>', 'mesh', 'networks', '</s>'], ['<s>', 'network', 'management', '</s>'], ['<s>', 'distributed', 'programming', '</s>']] 
Real Target Copy:   
		[['sensors', 'sensor', 'networks', '</s>'], ['java', '</s>'], ['mesh', 'networks', '</s>'], ['network', 'management', '</s>'], ['distributed', 'programming', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=0/5
		sensors sensor networks
		java
		mesh networks
		network management
		distributed programming
oov_list:   
		['sun1', 'java1'] 
[PREDICTION] #(valid)=126, #(present)=126, #(retained&present)=126, #(all)=126
		[3.6810]	*[paper] 	 [28, 2] 
		[4.1054]	*[performance] 	 [45, 2] 
		[4.1966]	*[compilation] 	 [2588, 2] 
		[4.3689]	*[systems] 	 [41, 2] 
		[4.4835]	*[addressing] 	 [2512, 2] 
		[4.5070]	*[implementation] 	 [186, 2] 
		[4.5641]	*[benchmark] 	 [1007, 2] 
		[4.7139]	*[memorialized] 	 [3887, 2] 
		[4.7191]	*[effect] 	 [311, 2] 
		[4.7211]	*[vectorization] 	 [5877, 2] 
		[4.7480]	*[predict] 	 [1134, 2] 
		[4.7909]	*[language] 	 [224, 2] 
		[4.8047]	*[operating system] 	 [891, 35, 2] 
		[4.8125]	*[method] 	 [34, 2] 
		[4.8385]	*[processor] 	 [804, 2] 
		[4.9121]	*[design] 	 [50, 2] 
		[4.9178]	*[use] 	 [65, 2] 
		[4.9493]	*[computation] 	 [313, 2] 
		[5.0401]	*[class] 	 [233, 2] 
		[5.0470]	*[analysis] 	 [43, 2] 
		[5.0486]	*[correctness] 	 [2060, 2] 
		[5.1247]	*[timing] 	 [998, 2] 
		[5.1373]	*[cache coherence] 	 [1010, 2528, 2] 
		[5.1394]	*[compiler] 	 [1566, 2] 
		[5.2038]	*[data] 	 [29, 2] 
		[5.3092]	*[instruction] 	 [1329, 2] 
		[5.4013]	*[code] 	 [230, 2] 
		[5.4125]	*[parallel] 	 [193, 2] 
		[5.5637]	*[embedded systems] 	 [584, 41, 2] 
		[5.7440]	*[programming language] 	 [160, 224, 2] 
		[5.9367]	*[operating systems] 	 [891, 41, 2] 
		[5.9700]	*[memory] 	 [225, 2] 
		[6.2873]	*[cache] 	 [1010, 2] 
		[6.3640]	*[software] 	 [98, 2] 
		[6.4280]	*[performance evaluation] 	 [45, 172, 2] 
		[6.4715]	*[embedded] 	 [584, 2] 
		[6.5576]	*[memory hierarchies] 	 [225, 3789, 2] 
		[6.6496]	*[instruction level parallelism] 	 [1329, 108, 1708, 2] 
		[6.6665]	*[software engineering] 	 [98, 416, 2] 
		[6.6702]	*[data integration] 	 [29, 437, 2] 
		[6.8372]	*[programming languages] 	 [160, 598, 2] 
		[6.8745]	*[memory management] 	 [225, 144, 2] 
		[6.9057]	*[performance analysis] 	 [45, 43, 2] 
		[6.9467]	*[programming] 	 [160, 2] 
		[7.0647]	*[code generation] 	 [230, 377, 2] 
		[7.2724]	*[software architecture] 	 [98, 212, 2] 
		[7.2837]	*[compiler optimization] 	 [1566, 95, 2] 
		[7.4046]	*[programming environments] 	 [160, 432, 2] 
		[7.4219]	*[embedded system] 	 [584, 35, 2] 
		[7.4992]	*[data processing] 	 [29, 192, 2] 
		[7.5270]	*[operating] 	 [891, 2] 
		[7.5491]	*[programming environment] 	 [160, 227, 2] 
		[7.6546]	*[data mining] 	 [29, 409, 2] 
		[7.6560]	*[memory scheduling] 	 [225, 263, 2] 
		[7.7000]	*[parallel programming] 	 [193, 160, 2] 
		[7.7050]	*[cache utilization] 	 [1010, 1578, 2] 
		[7.7985]	*[instruction caches] 	 [1329, 3613, 2] 
		[7.8129]	*[performance measurement] 	 [45, 588, 2] 
		[7.8823]	*[memory system] 	 [225, 35, 2] 
		[7.9033]	*[memory queries] 	 [225, 639, 2] 
		[8.0233]	*[performance optimization] 	 [45, 95, 2] 
		[8.0557]	*[data retrieval] 	 [29, 323, 2] 
		[9.4991]	*[analysis of design] 	 [43, 6, 50, 2] 
		[9.7565]	*[cache coherence protocol] 	 [1010, 2528, 342, 2] 
		[10.1781]	*[programming language systems] 	 [160, 224, 41, 2] 
		[10.6645]	*[operating system development] 	 [891, 35, 155, 2] 
		[10.7142]	*[data driven design] 	 [29, 655, 50, 2] 
		[10.7973]	*[data driven retrieval] 	 [29, 655, 323, 2] 
		[10.8033]	*[analysis of systems] 	 [43, 6, 41, 2] 
		[10.8348]	*[memory management systems] 	 [225, 144, 41, 2] 
		[10.8992]	*[analysis of languages] 	 [43, 6, 598, 2] 
		[10.9087]	*[software engineering design] 	 [98, 416, 50, 2] 
		[10.9462]	*[cache coherence algorithm] 	 [1010, 2528, 37, 2] 
		[10.9576]	*[analysis of algorithms] 	 [43, 6, 75, 2] 
		[10.9584]	*[operating system design] 	 [891, 35, 50, 2] 
		[11.0774]	*[cache coherence analysis] 	 [1010, 2528, 43, 2] 
		[11.0948]	*[data driven systems] 	 [29, 655, 41, 2] 
		[11.0993]	*[cache coherence problem] 	 [1010, 2528, 44, 2] 
		[11.1384]	*[operating system interoperability] 	 [891, 35, 1936, 2] 
		[11.1558]	*[data driven] 	 [29, 655, 2] 
		[11.1677]	*[instruction level] 	 [1329, 108, 2] 
		[11.3725]	*[data driven simulation] 	 [29, 655, 93, 2] 
		[11.3776]	*[software engineering curriculum] 	 [98, 416, 2911, 2] 
		[11.3777]	*[memory management system] 	 [225, 144, 35, 2] 
		[11.4529]	*[operating system systems] 	 [891, 35, 41, 2] 
		[11.4867]	*[operating system implementation] 	 [891, 35, 186, 2] 
		[11.5271]	*[cache coherence time] 	 [1010, 2528, 38, 2] 
		[11.6521]	*[analysis of memory] 	 [43, 6, 225, 2] 
		[12.1181]	*[performance evaluation and design] 	 [45, 172, 8, 50, 2] 
		[12.5459]	*[analysis of user] 	 [43, 6, 103, 2] 
		[12.5742]	*[data integration and retrieval] 	 [29, 437, 8, 323, 2] 
		[12.6078]	*[analysis of] 	 [43, 6, 2] 
		[12.9115]	*[performance analysis and design] 	 [45, 43, 8, 50, 2] 
		[13.1469]	*[code generation and retrieval] 	 [230, 377, 8, 323, 2] 
		[13.2805]	*[analysis of memory systems] 	 [43, 6, 225, 41, 2] 
		[13.2999]	*[code generation and design] 	 [230, 377, 8, 50, 2] 
		[13.4123]	*[performance evaluation and analysis] 	 [45, 172, 8, 43, 2] 
		[13.4133]	*[operating system and design] 	 [891, 35, 8, 50, 2] 
		[13.4450]	*[performance evaluation and evaluation] 	 [45, 172, 8, 172, 2] 
		[13.5116]	*[analysis of memory system] 	 [43, 6, 225, 35, 2] 
		[13.6964]	*[performance evaluation and management] 	 [45, 172, 8, 144, 2] 
		[13.7318]	*[analysis of user interfaces] 	 [43, 6, 103, 790, 2] 
		[13.8179]	*[code generation and analysis] 	 [230, 377, 8, 43, 2] 
		[13.9019]	*[performance evaluation and] 	 [45, 172, 8, 2] 
		[13.9306]	*[analysis of user interface] 	 [43, 6, 103, 319, 2] 
		[13.9995]	*[performance analysis and analysis] 	 [45, 43, 8, 43, 2] 
		[14.0531]	*[performance evaluation and control] 	 [45, 172, 8, 66, 2] 
		[14.1013]	*[instruction level parallelism systems] 	 [1329, 108, 1708, 41, 2] 
		[14.1465]	*[code generation and systems] 	 [230, 377, 8, 41, 2] 
		[14.1728]	*[performance evaluation and discovery] 	 [45, 172, 8, 969, 2] 
		[14.3346]	*[operating system and systems] 	 [891, 35, 8, 41, 2] 
		[14.3463]	*[code generation and indexing] 	 [230, 377, 8, 1293, 2] 
		[14.3528]	*[performance evaluation and retrieval] 	 [45, 172, 8, 323, 2] 
		[14.3919]	*[performance evaluation and development] 	 [45, 172, 8, 155, 2] 
		[14.4100]	*[programming language and design] 	 [160, 224, 8, 50, 2] 
		[14.4200]	*[performance analysis and evaluation] 	 [45, 43, 8, 172, 2] 
		[14.4227]	*[data integration and design] 	 [29, 437, 8, 50, 2] 
		[14.5194]	*[performance analysis and] 	 [45, 43, 8, 2] 
		[14.5704]	*[operating system and evaluation] 	 [891, 35, 8, 172, 2] 
		[15.4683]	*[code generation and] 	 [230, 377, 8, 2] 
		[16.1987]	*[programming language and] 	 [160, 224, 8, 2] 
		[16.5585]	*[operating system and user] 	 [891, 35, 8, 103, 2] 
		[17.0896]	*[data integration and] 	 [29, 437, 8, 2] 
		[17.8788]	*[data driven design and] 	 [29, 655, 50, 8, 2] 
		[18.1104]	*[instruction level parallelism and] 	 [1329, 108, 1708, 8, 2] 
		[18.9548]	*[analysis of design and] 	 [43, 6, 50, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.02452316076294278 , 0.03454946506717624 , 0.026542434403427653
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.01634877384196185 , 0.044075437127208246 , 0.02189629815142351
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.04643008554257879 , 0.0685404247533375 , 0.050548004573634966
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.03783162526319397 , 0.11096537471418902 , 0.05207089782709072
 =======================================================