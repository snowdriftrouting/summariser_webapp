[Source][178]: a rapid mapping conversion methodology to a commercial off the shelf system . the adoption of commercial off the shelf ( cots ) packages has benefits , but it also has challenges and pitfalls . the conversion of a legacy system to a new system based on a cots package demands a dedicated guidance . this paper proposes a new methodology , sucada ( simplified use case driven approach ) , that provides step by step guidelines for the conversion of a legacy system to a cots package . the methodology uses an iterative approach to converting legacy systems to a cots package through the use of simplified business use cases . it aims for rapid mapping of the functional requirements of the legacy system to a cots package . the methodology also provides strategies to deal with legacy functionality not supported by a cots package . this paper also presents the findings of a case study on the application of the proposed methodology for the conversion of a legacy laboratory information management system ( lims ) . 

Source Input: 
 <s> a rapid mapping conversion methodology to a commercial off the shelf system . the adoption of commercial off the shelf ( cots ) packages has benefits , but it also has challenges and pitfalls . the conversion of a legacy system to a new system based on a cots package demands a dedicated guidance . this paper proposes a new methodology , <unk> ( simplified use case driven approach ) , that provides step by step guidelines for the conversion of a legacy system to a cots package . the methodology uses an iterative approach to converting legacy systems to a cots package through the use of simplified business use cases . it aims for rapid mapping of the functional requirements of the legacy system to a cots package . the methodology also provides strategies to deal with legacy functionality not supported by a cots package . this paper also presents the findings of a case study on the application of the proposed methodology for the conversion of a legacy laboratory information management system ( <unk> ) . </s>
Real Target String [8] 
		[['sucada'], ['cots'], ['system', 'development'], ['methodology'], ['conversion', 'of', 'legacy', 'system'], ['use', 'cases'], ['product', 'evaluation', 'and', 'selection'], ['process', 'guidance']] 
Real Target Input:  
		[['<s>', '<unk>', '</s>'], ['<s>', 'cots', '</s>'], ['<s>', 'system', 'development', '</s>'], ['<s>', 'methodology', '</s>'], ['<s>', 'conversion', 'of', 'legacy', 'system', '</s>'], ['<s>', 'use', 'cases', '</s>'], ['<s>', 'product', 'evaluation', 'and', 'selection', '</s>'], ['<s>', 'process', 'guidance', '</s>']] 
Real Target Copy:   
		[['sucada', '</s>'], ['cots', '</s>'], ['system', 'development', '</s>'], ['methodology', '</s>'], ['conversion', 'of', 'legacy', 'system', '</s>'], ['use', 'cases', '</s>'], ['product', 'evaluation', 'and', 'selection', '</s>'], ['process', 'guidance', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=4/8
		[sucada]
		[cots]
		system development
		[methodology]
		conversion of legacy system
		[use cases]
		product evaluation and selection
		process guidance
oov_list:   
		['sucada', 'lims'] 
[PREDICTION] #(valid)=104, #(present)=104, #(retained&present)=104, #(all)=104
		[4.7756]	*[simulation] 	 [93, 2] 
		[4.7932]	*[decidability] 	 [5187, 2] 
		[5.0818]	*[decision support system] 	 [173, 130, 35, 2] 
		[5.5151]	*[multi agent systems] 	 [100, 465, 41, 2] 
		[5.5364]	*[computer animation] 	 [157, 1747, 2] 
		[5.6136]	*[decision making] 	 [173, 428, 2] 
		[5.6416]	*[risk assessment] 	 [599, 671, 2] 
		[5.8137]	*[decision support systems] 	 [173, 130, 41, 2] 
		[5.9493]	*[e learning] 	 [129, 77, 2] 
		[6.2595]	*[quality of service] 	 [141, 6, 162, 2] 
		[6.3151]	*[system buses] 	 [35, 6434, 2] 
		[6.3435]	*[automotive] 	 [4190, 2] 
		[6.4120]	*[software engineering] 	 [98, 416, 2] 
		[6.4634]	*[genetic algorithm] 	 [340, 37, 2] 
		[6.4851]	*[non natives] 	 [123, 16916, 2] 
		[6.5235]	*[genetic algorithms] 	 [340, 75, 2] 
		[6.5370]	*[corporate governance] 	 [4067, 3876, 2] 
		[6.6258]	*[multi agent system] 	 [100, 465, 35, 2] 
		[6.6854]	*[decision support] 	 [173, 130, 2] 
		[6.7699]	*[supply chain management] 	 [719, 596, 144, 2] 
		[7.0009]	*[decision theory] 	 [173, 179, 2] 
		[7.0035]	*[collaborative filtering] 	 [682, 800, 2] 
		[7.0091]	*[self organization] 	 [339, 1252, 2] 
		[7.0609]	*[knowledge acquisition] 	 [134, 1575, 2] 
		[7.0705]	*[model] 	 [30, 2] 
		[7.1047]	*[decision analysis] 	 [173, 43, 2] 
		[7.1243]	*[multi agent simulation] 	 [100, 465, 93, 2] 
		[7.2040]	*[supply chain] 	 [719, 596, 2] 
		[7.2272]	*[self stabilization] 	 [339, 3091, 2] 
		[7.3201]	*[learning] 	 [77, 2] 
		[7.3228]	*[virtual environment] 	 [312, 227, 2] 
		[7.3343]	*[computational complexity] 	 [201, 211, 2] 
		[7.3361]	*[knowledge based systems] 	 [134, 26, 41, 2] 
		[7.3512]	*[multi agent] 	 [100, 465, 2] 
		[7.3549]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[7.4407]	*[artificial neural network] 	 [661, 184, 52, 2] 
		[7.9045]	*[open] 	 [551, 2] 
		[7.9747]	*[risk] 	 [599, 2] 
		[7.9999]	*[open source] 	 [551, 336, 2] 
		[8.0263]	*[decision] 	 [173, 2] 
		[8.0762]	*[construction] 	 [666, 2] 
		[8.1013]	*[knowledge based system] 	 [134, 26, 35, 2] 
		[8.1266]	*[artificial neural networks] 	 [661, 184, 62, 2] 
		[8.1369]	*[business process modeling] 	 [541, 73, 148, 2] 
		[8.1895]	*[software] 	 [98, 2] 
		[8.2459]	*[business process re engineering] 	 [541, 73, 1222, 416, 2] 
		[8.3080]	*[business] 	 [541, 2] 
		[8.3823]	*[system] 	 [35, 2] 
		[8.5214]	*[adaptive] 	 [194, 2] 
		[8.5803]	*[quality] 	 [141, 2] 
		[8.6282]	*[business process modelling] 	 [541, 73, 516, 2] 
		[8.6642]	*[knowledge] 	 [134, 2] 
		[8.7634]	*[self organizing maps] 	 [339, 2029, 979, 2] 
		[8.9019]	*[business process system] 	 [541, 73, 35, 2] 
		[8.9352]	*[multi objective optimization] 	 [100, 349, 95, 2] 
		[8.9586]	*[decision making system] 	 [173, 428, 35, 2] 
		[8.9595]	*[universal] 	 [1938, 2] 
		[9.1180]	*[supply chain system] 	 [719, 596, 35, 2] 
		[9.1802]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[9.2092]	*[supply chain design] 	 [719, 596, 50, 2] 
		[9.2208]	*[electronic] 	 [786, 2] 
		[9.2224]	*[universal audiovisual] 	 [1938, 7158, 2] 
		[9.2688]	*[supply chain planning] 	 [719, 596, 552, 2] 
		[9.3197]	*[model based testing] 	 [30, 26, 475, 2] 
		[9.3464]	*[self organizing map] 	 [339, 2029, 578, 2] 
		[9.3510]	*[decision support simulation] 	 [173, 130, 93, 2] 
		[9.3648]	*[multi agent learning] 	 [100, 465, 77, 2] 
		[9.4060]	*[computer] 	 [157, 2] 
		[9.4283]	*[supply chain systems] 	 [719, 596, 41, 2] 
		[9.4586]	*[collaborative] 	 [682, 2] 
		[9.5823]	*[business process systems] 	 [541, 73, 41, 2] 
		[9.6723]	*[multi agent decision making] 	 [100, 465, 173, 428, 2] 
		[9.9020]	*[business process] 	 [541, 73, 2] 
		[10.0056]	*[e] 	 [129, 2] 
		[10.0799]	*[multi user] 	 [100, 103, 2] 
		[10.1345]	*[human] 	 [252, 2] 
		[10.1479]	*[genetic] 	 [340, 2] 
		[10.1678]	*[model based design] 	 [30, 26, 50, 2] 
		[10.6894]	*[multi objective] 	 [100, 349, 2] 
		[10.7290]	*[supply chain management system] 	 [719, 596, 144, 35, 2] 
		[10.9195]	*[computer aided] 	 [157, 2031, 2] 
		[10.9215]	*[knowledge based] 	 [134, 26, 2] 
		[10.9587]	*[self organizing] 	 [339, 2029, 2] 
		[11.1877]	*[multi agent systems engineering] 	 [100, 465, 41, 416, 2] 
		[11.2211]	*[decision support system design] 	 [173, 130, 35, 50, 2] 
		[11.3409]	*[multi agent system design] 	 [100, 465, 35, 50, 2] 
		[11.3638]	*[decision support system development] 	 [173, 130, 35, 155, 2] 
		[11.4993]	*[decision support system system] 	 [173, 130, 35, 35, 2] 
		[11.4997]	*[multi agent systems design] 	 [100, 465, 41, 50, 2] 
		[11.5657]	*[decision support system systems] 	 [173, 130, 35, 41, 2] 
		[11.6208]	*[supply] 	 [719, 2] 
		[11.7233]	*[model based] 	 [30, 26, 2] 
		[11.7756]	*[multi agent systems optimization] 	 [100, 465, 41, 95, 2] 
		[11.8584]	*[decision support system engineering] 	 [173, 130, 35, 416, 2] 
		[11.8620]	*[quality of service development] 	 [141, 6, 162, 155, 2] 
		[11.8691]	*[decision support system tool] 	 [173, 130, 35, 295, 2] 
		[11.8767]	*[multi agent system engineering] 	 [100, 465, 35, 416, 2] 
		[12.9690]	*[artificial neural] 	 [661, 184, 2] 
		[14.2189]	*[risk assessment and] 	 [599, 671, 8, 2] 
		[14.7708]	*[supply chain planning and] 	 [719, 596, 552, 8, 2] 
		[14.8233]	*[quality of service and] 	 [141, 6, 162, 8, 2] 
		[14.8732]	*[quality of service of] 	 [141, 6, 162, 6, 2] 
		[15.1800]	*[multi agent decision] 	 [100, 465, 173, 2] 
		[15.4503]	*[supply chain design and] 	 [719, 596, 50, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.02 , 0.018095238095238095 , 0.016313131313131314
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.016 , 0.033761904761904764 , 0.019497204549836125
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.049380953073501585 , 0.05757142953290826 , 0.04836552679330154
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.04070952433347702 , 0.11655555736238048 , 0.05590475100790786
 =======================================================