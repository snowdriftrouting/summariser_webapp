[Source][81]: speculation on the generality of the backward stepwise view of pca . a novel backwards viewpoint of principal component analysis is proposed . in a wide variety of cases , that fall into the area of object oriented data analysis , this viewpoint is seen to provide much more natural and accessable analogs of pca than the standard forward viewpoint . examples considered here include principal curves , landmark based shape analysis , medial shape representation and trees as data . 

Source Input: 
 <s> speculation on the generality of the backward stepwise view of pca . a novel backwards viewpoint of principal component analysis is proposed . in a wide variety of cases , that fall into the area of object oriented data analysis , this viewpoint is seen to provide much more natural and <unk> analogs of pca than the standard forward viewpoint . examples considered here include principal curves , landmark based shape analysis , medial shape representation and trees as data . </s> <pad> <pad> <pad>
Real Target String [1] 
		[['principal', 'component', 'analysis']] 
Real Target Input:  
		[['<s>', 'principal', 'component', 'analysis', '</s>']] 
Real Target Copy:   
		[['principal', 'component', 'analysis', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=1/1
		[principal component analysis]
oov_list:   
		['accessable'] 
[PREDICTION] #(valid)=115, #(present)=115, #(retained&present)=115, #(all)=115
		[4.9910]	*[imaging] 	 [987, 2] 
		[5.2141]	*[computer animation] 	 [157, 1747, 2] 
		[5.3396]	*[principal component analysis] 	 [1288, 414, 43, 2] [correct!]
		[5.3743]	*[mri] 	 [2742, 2] 
		[5.4141]	*[super resolution] 	 [2505, 656, 2] 
		[5.4881]	*[registration] 	 [1621, 2] 
		[5.5436]	*[image segmentation] 	 [89, 421, 2] 
		[5.6670]	*[image processing] 	 [89, 192, 2] 
		[5.6867]	*[motion estimation] 	 [321, 223, 2] 
		[5.8144]	*[icp] 	 [8030, 2] 
		[5.8489]	*[surface fitting] 	 [317, 1762, 2] 
		[5.8991]	*[deformable models] 	 [2565, 69, 2] 
		[5.9435]	*[face recognition] 	 [515, 255, 2] 
		[6.0238]	*[computer vision] 	 [157, 1051, 2] 
		[6.0476]	*[anisotropic diffusion] 	 [2626, 783, 2] 
		[6.1903]	*[shape optimization] 	 [334, 95, 2] 
		[6.2689]	*[surface roughness] 	 [317, 3230, 2] 
		[6.3210]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[6.3538]	*[3d] 	 [305, 2] 
		[6.3600]	*[active contour] 	 [573, 1832, 2] 
		[6.5071]	*[3d ladar scanning] 	 [305, 47817, 2770, 2] 
		[6.5445]	*[structure] 	 [143, 2] 
		[6.5961]	*[shape priors] 	 [334, 6145, 2] 
		[6.6290]	*[image restoration] 	 [89, 2977, 2] 
		[6.6537]	*[motion artefact] 	 [321, 13325, 2] 
		[6.7300]	*[image registration] 	 [89, 1621, 2] 
		[6.7783]	*[level set] 	 [108, 79, 2] 
		[6.7988]	*[bzier curves] 	 [7307, 1316, 2] 
		[6.8000]	*[3d analysis] 	 [305, 43, 2] 
		[6.8083]	*[template matching] 	 [1966, 454, 2] 
		[6.8180]	*[adaptive meshing] 	 [194, 6276, 2] 
		[6.8212]	*[feature extraction] 	 [187, 591, 2] 
		[6.8252]	*[signal processing] 	 [371, 192, 2] 
		[6.8656]	*[image denoising] 	 [89, 2827, 2] 
		[6.8709]	*[motion] 	 [321, 2] 
		[6.9771]	*[surface] 	 [317, 2] 
		[7.0083]	*[shape] 	 [334, 2] 
		[7.2783]	*[feature] 	 [187, 2] 
		[7.3933]	*[deformable] 	 [2565, 2] 
		[7.5666]	*[level set method] 	 [108, 79, 34, 2] 
		[7.5746]	*[image] 	 [89, 2] 
		[7.6147]	*[level of detail] 	 [108, 6, 1332, 2] 
		[7.7326]	*[volumetric] 	 [3228, 2] 
		[7.7432]	*[3d shape] 	 [305, 334, 2] 
		[7.7753]	*[3d shape optimization] 	 [305, 334, 95, 2] 
		[7.8838]	*[adaptive] 	 [194, 2] 
		[7.8893]	*[3d ladar modulation] 	 [305, 47817, 1664, 2] 
		[8.0516]	*[nuclei] 	 [6272, 2] 
		[8.1771]	*[3d shape models] 	 [305, 334, 69, 2] 
		[8.2860]	*[3d ladar sounds] 	 [305, 47817, 5545, 2] 
		[8.4773]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[8.5882]	*[template] 	 [1966, 2] 
		[8.5900]	*[active shape] 	 [573, 334, 2] 
		[8.5939]	*[3d shape analysis] 	 [305, 334, 43, 2] 
		[8.6275]	*[level] 	 [108, 2] 
		[8.6492]	*[nuclei chromatin] 	 [6272, 10040, 2] 
		[8.7341]	*[3d ladar lasers] 	 [305, 47817, 7545, 2] 
		[8.8202]	*[active shape models] 	 [573, 334, 69, 2] 
		[8.8353]	*[roc] 	 [4794, 2] 
		[8.8513]	*[bzier] 	 [7307, 2] 
		[8.9325]	*[active contour models] 	 [573, 1832, 69, 2] 
		[8.9570]	*[3d ladar angiography] 	 [305, 47817, 6828, 2] 
		[8.9851]	*[face] 	 [515, 2] 
		[8.9960]	*[3d ladar imaging] 	 [305, 47817, 987, 2] 
		[9.0125]	*[3d ladar diode] 	 [305, 47817, 5195, 2] 
		[9.1512]	*[3d ladar acid] 	 [305, 47817, 3022, 2] 
		[9.1885]	*[3d shape tracking] 	 [305, 334, 480, 2] 
		[9.2271]	*[3d ladar disease] 	 [305, 47817, 1200, 2] 
		[9.2740]	*[level set methods] 	 [108, 79, 74, 2] 
		[9.2869]	*[3d ladar mask] 	 [305, 47817, 3176, 2] 
		[9.3225]	*[3d ladar lithography] 	 [305, 47817, 3006, 2] 
		[9.3381]	*[active shape optimization] 	 [573, 334, 95, 2] 
		[9.3480]	*[3d shape correlation] 	 [305, 334, 652, 2] 
		[9.3568]	*[level of squares] 	 [108, 6, 1289, 2] 
		[9.4185]	*[3d shape estimation] 	 [305, 334, 223, 2] 
		[9.4573]	*[3d shape detection] 	 [305, 334, 161, 2] 
		[9.5382]	*[3d ladar sequences] 	 [305, 47817, 554, 2] 
		[9.5984]	*[3d shape modeling] 	 [305, 334, 148, 2] 
		[9.6881]	*[3d ladar offset] 	 [305, 47817, 2818, 2] 
		[9.7381]	*[signal] 	 [371, 2] 
		[9.7727]	*[spectral] 	 [874, 2] 
		[9.8196]	*[active] 	 [573, 2] 
		[9.9056]	*[principal component] 	 [1288, 414, 2] 
		[9.9975]	*[computer aided] 	 [157, 2031, 2] 
		[10.0256]	*[3d ladar] 	 [305, 47817, 2] 
		[10.0423]	*[level of detail optimization] 	 [108, 6, 1332, 95, 2] 
		[10.3514]	*[computer] 	 [157, 2] 
		[10.6976]	*[magnetic] 	 [1047, 2] 
		[10.8880]	*[level of detail models] 	 [108, 6, 1332, 69, 2] 
		[11.1821]	*[level of fuzzy set] 	 [108, 6, 83, 79, 2] 
		[11.2802]	*[level of detail algorithms] 	 [108, 6, 1332, 75, 2] 
		[11.2978]	*[level of detail algorithm] 	 [108, 6, 1332, 37, 2] 
		[11.3691]	*[level of detail system] 	 [108, 6, 1332, 35, 2] 
		[11.5500]	*[level of detail expansion] 	 [108, 6, 1332, 1386, 2] 
		[11.5670]	*[level of detail analysis] 	 [108, 6, 1332, 43, 2] 
		[11.6888]	*[level of detail constraints] 	 [108, 6, 1332, 306, 2] 
		[11.8664]	*[level of detail method] 	 [108, 6, 1332, 34, 2] 
		[11.9922]	*[level of detail estimation] 	 [108, 6, 1332, 223, 2] 
		[12.1308]	*[level of detail model] 	 [108, 6, 1332, 30, 2] 
		[12.1408]	*[level of detail processing] 	 [108, 6, 1332, 192, 2] 
		[12.3148]	*[level of fuzzy logic] 	 [108, 6, 83, 287, 2] 
		[12.5137]	*[computer aided diagnosis system] 	 [157, 2031, 997, 35, 2] 
		[12.5887]	*[level of detail systems] 	 [108, 6, 1332, 41, 2] 
		[12.6716]	*[computer aided diagnosis algorithms] 	 [157, 2031, 997, 75, 2] 
		[12.7227]	*[level of detail characterization] 	 [108, 6, 1332, 1193, 2] 
		[12.7398]	*[level of detail theory] 	 [108, 6, 1332, 179, 2] 
		[12.7788]	*[level of detail shop] 	 [108, 6, 1332, 3280, 2] 
		[12.7839]	*[level of detail mapping] 	 [108, 6, 1332, 601, 2] 
		[12.8126]	*[level of fuzzy optimization] 	 [108, 6, 83, 95, 2] 
		[12.8396]	*[level of fuzzy models] 	 [108, 6, 83, 69, 2] 
		[12.8486]	*[level of detail measure] 	 [108, 6, 1332, 434, 2] 
		[12.9479]	*[level of fuzzy] 	 [108, 6, 83, 2] 
		[15.5941]	*[computer aided diagnosis and] 	 [157, 2031, 997, 8, 2] 
		[16.2381]	*[computer aided design and] 	 [157, 2031, 50, 8, 2] 
		[16.5277]	*[principal component analysis and] 	 [1288, 414, 43, 8, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.2 , 1.0 , 0.33333333333333337
 --- total precision, recall, fscore: 0.025416666666666664 , 0.03755821724571725 , 0.026966093627519713
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.1 , 1.0 , 0.18181818181818182
 --- total precision, recall, fscore: 0.017083333333333336 , 0.04661482421899089 , 0.022241825230458662
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.2 , 1.0 , 0.33333333333333337
 --- total precision, recall, fscore: 0.048631614139303565 , 0.07638375136894007 , 0.05340358451098135
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.1 , 1.0 , 0.18181818181818182
 --- total precision, recall, fscore: 0.03904001357809951 , 0.11898450633230141 , 0.053700351655876354
 =======================================================