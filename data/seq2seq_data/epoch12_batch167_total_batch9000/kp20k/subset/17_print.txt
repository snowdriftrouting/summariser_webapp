[Source][68]: lightweight formalization and validation of orm models . a formalization of orm modeling language in alloy is proposed . we propose a method to check satisfiability of an orm model using its alloy meta model . we propose a numeric meta model to improve scalability of checking . various unsatisfiability patterns are formalized in alloy . various mid sized real world models are checked as case study . 

Source Input: 
 <s> lightweight formalization and validation of orm models . a formalization of orm modeling language in alloy is proposed . we propose a method to check satisfiability of an orm model using its alloy meta model . we propose a numeric meta model to improve scalability of checking . various unsatisfiability patterns are formalized in alloy . various mid sized real world models are checked as case study . </s> <pad> <pad> <pad>
Real Target String [3] 
		[['formal', 'specification'], ['object', 'role', 'modeling'], ['satisfiability', 'checking']] 
Real Target Input:  
		[['<s>', 'formal', 'specification', '</s>'], ['<s>', 'object', 'role', 'modeling', '</s>'], ['<s>', 'satisfiability', 'checking', '</s>']] 
Real Target Copy:   
		[['formal', 'specification', '</s>'], ['object', 'role', 'modeling', '</s>'], ['satisfiability', 'checking', '</s>']] 
[GROUND-TRUTH] #(present)/#(all targets)=0/3
		formal specification
		object role modeling
		satisfiability checking
oov_list:   
		[] 
[PREDICTION] #(valid)=108, #(present)=108, #(retained&present)=108, #(all)=108
		[5.0513]	*[quality of service] 	 [141, 6, 162, 2] 
		[5.3543]	*[simulation] 	 [93, 2] 
		[5.3812]	*[decision support system] 	 [173, 130, 35, 2] 
		[5.3883]	*[computer animation] 	 [157, 1747, 2] 
		[5.3932]	*[risk assessment] 	 [599, 671, 2] 
		[5.6091]	*[decidability] 	 [5187, 2] 
		[5.7223]	*[non natives] 	 [123, 16916, 2] 
		[5.7698]	*[e learning] 	 [129, 77, 2] 
		[5.7826]	*[comparative study] 	 [1637, 67, 2] 
		[5.8140]	*[prediction] 	 [380, 2] 
		[5.9322]	*[multiple stressors] 	 [125, 14366, 2] 
		[5.9853]	*[knowledge acquisition] 	 [134, 1575, 2] 
		[6.1574]	*[decision making] 	 [173, 428, 2] 
		[6.4686]	*[decision support systems] 	 [173, 130, 41, 2] 
		[6.5346]	*[multi agent systems] 	 [100, 465, 41, 2] 
		[6.6131]	*[model] 	 [30, 2] 
		[6.6262]	*[human computer interaction] 	 [252, 157, 253, 2] 
		[6.6401]	*[markov chain] 	 [752, 596, 2] 
		[6.6478]	*[model checking] 	 [30, 1069, 2] 
		[6.7099]	*[multi stressors] 	 [100, 14366, 2] 
		[6.8020]	*[decision support] 	 [173, 130, 2] 
		[6.9279]	*[patent infringement] 	 [2658, 16100, 2] 
		[6.9298]	*[model driven engineering] 	 [30, 655, 416, 2] 
		[6.9457]	*[knowledge representation] 	 [134, 309, 2] 
		[6.9560]	*[virtual characters] 	 [312, 2910, 2] 
		[7.0502]	*[learning] 	 [77, 2] 
		[7.0574]	*[computer vision] 	 [157, 1051, 2] 
		[7.0768]	*[quality assessment] 	 [141, 671, 2] 
		[7.1120]	*[knowledge based systems] 	 [134, 26, 41, 2] 
		[7.1528]	*[intelligent tutoring system] 	 [843, 5669, 35, 2] 
		[7.1538]	*[intelligent tutoring systems] 	 [843, 5669, 41, 2] 
		[7.1581]	*[visual impairment] 	 [399, 5792, 2] 
		[7.2480]	*[quality] 	 [141, 2] 
		[7.2835]	*[human robot interaction] 	 [252, 629, 253, 2] 
		[7.3196]	*[information integration] 	 [47, 437, 2] 
		[7.3256]	*[markov chain monte carlo] 	 [752, 596, 1225, 1226, 2] 
		[7.5321]	*[virtual humans] 	 [312, 2830, 2] 
		[7.5548]	*[multi agent system] 	 [100, 465, 35, 2] 
		[7.6518]	*[knowledge based system] 	 [134, 26, 35, 2] 
		[7.7081]	*[risk] 	 [599, 2] 
		[7.8483]	*[multi agent simulation] 	 [100, 465, 93, 2] 
		[7.9766]	*[multi agent] 	 [100, 465, 2] 
		[8.0020]	*[human action recognition] 	 [252, 1063, 255, 2] 
		[8.0222]	*[computer aided design] 	 [157, 2031, 50, 2] 
		[8.0607]	*[knowledge] 	 [134, 2] 
		[8.0966]	*[human motion analysis] 	 [252, 321, 43, 2] 
		[8.1899]	*[model based testing] 	 [30, 26, 475, 2] 
		[8.2484]	*[decision] 	 [173, 2] 
		[8.3199]	*[intelligent tutoring] 	 [843, 5669, 2] 
		[8.4622]	*[computer aided diagnosis] 	 [157, 2031, 997, 2] 
		[8.5347]	*[knowledge based animation] 	 [134, 26, 1747, 2] 
		[8.5537]	*[business process modeling] 	 [541, 73, 148, 2] 
		[8.5868]	*[human motion] 	 [252, 321, 2] 
		[8.6405]	*[construction] 	 [666, 2] 
		[8.6812]	*[quality of testing] 	 [141, 6, 475, 2] 
		[8.7346]	*[adaptive] 	 [194, 2] 
		[8.7477]	*[business] 	 [541, 2] 
		[8.8539]	*[open] 	 [551, 2] 
		[8.9046]	*[information] 	 [47, 2] 
		[8.9293]	*[business process re engineering] 	 [541, 73, 1222, 416, 2] 
		[9.0349]	*[patent infringement analysis] 	 [2658, 16100, 43, 2] 
		[9.0485]	*[human action] 	 [252, 1063, 2] 
		[9.1493]	*[human] 	 [252, 2] 
		[9.3553]	*[video] 	 [215, 2] 
		[9.4199]	*[business process modelling] 	 [541, 73, 516, 2] 
		[9.4331]	*[quality of science] 	 [141, 6, 770, 2] 
		[9.4500]	*[human computer interface] 	 [252, 157, 319, 2] 
		[9.4656]	*[model driven development] 	 [30, 655, 155, 2] 
		[9.4664]	*[model based engineering] 	 [30, 26, 416, 2] 
		[9.5008]	*[electronic] 	 [786, 2] 
		[9.5181]	*[knowledge based analysis] 	 [134, 26, 43, 2] 
		[9.5230]	*[decision support simulation] 	 [173, 130, 93, 2] 
		[9.6606]	*[patent] 	 [2658, 2] 
		[10.0178]	*[knowledge based] 	 [134, 26, 2] 
		[10.0217]	*[markov] 	 [752, 2] 
		[10.0714]	*[computer] 	 [157, 2] 
		[10.2325]	*[e] 	 [129, 2] 
		[10.4354]	*[quality of service metastasis] 	 [141, 6, 162, 9669, 2] 
		[10.5148]	*[human computer] 	 [252, 157, 2] 
		[10.6425]	*[business process] 	 [541, 73, 2] 
		[10.7735]	*[human robot] 	 [252, 629, 2] 
		[10.7773]	*[quality of service model] 	 [141, 6, 162, 30, 2] 
		[10.8673]	*[intelligent tutoring system design] 	 [843, 5669, 35, 50, 2] 
		[10.9727]	*[intelligent tutoring systems engineering] 	 [843, 5669, 41, 416, 2] 
		[11.0644]	*[intelligent tutoring system development] 	 [843, 5669, 35, 155, 2] 
		[11.1811]	*[comparative] 	 [1637, 2] 
		[11.2199]	*[quality of service decay] 	 [141, 6, 162, 3728, 2] 
		[11.2313]	*[computer aided] 	 [157, 2031, 2] 
		[11.3089]	*[model based] 	 [30, 26, 2] 
		[11.3792]	*[quality of service rate] 	 [141, 6, 162, 209, 2] 
		[11.3914]	*[quality of service realization] 	 [141, 6, 162, 3159, 2] 
		[11.3963]	*[quality of service development] 	 [141, 6, 162, 155, 2] 
		[11.4061]	*[quality of service ratio] 	 [141, 6, 162, 566, 2] 
		[11.4074]	*[quality of service indicators] 	 [141, 6, 162, 3141, 2] 
		[11.4571]	*[model driven] 	 [30, 655, 2] 
		[11.5741]	*[quality of service spacing] 	 [141, 6, 162, 6055, 2] 
		[11.5820]	*[intelligent tutoring system engineering] 	 [843, 5669, 35, 416, 2] 
		[11.6449]	*[risk assessment and testing] 	 [599, 671, 8, 475, 2] 
		[11.6553]	*[quality of service analysis] 	 [141, 6, 162, 43, 2] 
		[11.6632]	*[quality of service checking] 	 [141, 6, 162, 1069, 2] 
		[11.6889]	*[quality of service protection] 	 [141, 6, 162, 1611, 2] 
		[11.7505]	*[quality of service defense] 	 [141, 6, 162, 4931, 2] 
		[11.7988]	*[quality of service engineering] 	 [141, 6, 162, 416, 2] 
		[11.8671]	*[decision support system design] 	 [173, 130, 35, 50, 2] 
		[13.6763]	*[quality of service of] 	 [141, 6, 162, 6, 2] 
		[13.7648]	*[quality of service and] 	 [141, 6, 162, 8, 2] 
		[13.7854]	*[risk assessment and] 	 [599, 671, 8, 2] 
		[16.3537]	*[intelligent tutoring system on] 	 [843, 5669, 35, 19, 2] 

 =======================================================
 ------------------------------------------------- EXACT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.03333333333333334 , 0.022486772486772486 , 0.020622895622895623
 ------------------------------------------------- EXACT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.03333333333333333 , 0.06600529100529101 , 0.038285885654306705
 ------------------------------------------------- SOFT, k=5
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.07203703787591723 , 0.061441799852425455 , 0.05746586319774089
 ------------------------------------------------- SOFT, k=10
 --- batch precision, recall, fscore: 0.0 , 0.0 , 0.0
 --- total precision, recall, fscore: 0.058240741160180826 , 0.12116402207464769 , 0.07093767238778421
 =======================================================